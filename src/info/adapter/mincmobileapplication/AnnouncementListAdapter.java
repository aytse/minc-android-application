package info.adapter.mincmobileapplication;

import info.main.mincmobileapplication.R;
import info.model.mincmobileapplication.AnnouncementItem;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AnnouncementListAdapter extends BaseAdapter{
	ArrayList<AnnouncementItem> announcements;
	Activity a;
	
	public AnnouncementListAdapter() {}
	
	public AnnouncementListAdapter(Activity a, ArrayList<AnnouncementItem> announcements) {
		this.announcements = announcements;
		this.a = a;
	}
	
	@Override
	public int getCount() {
		return announcements.size();
	}

	@Override
	public Object getItem(int position) {
		return announcements.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderAnnouncement holder;
		
		if(convertView == null) {
			holder = new ViewHolderAnnouncement();
			
			LayoutInflater inflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.course_announcement_element, parent, false);

			holder.announcementTitle = (TextView) convertView.findViewById(R.id.announcement_title);
		
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolderAnnouncement) convertView.getTag();
		}
		
		holder.announcementTitle.setText(announcements.get(position).getTitle());
		return convertView;
	}
	
	class ViewHolderAnnouncement {
		TextView announcementTitle;
	}

}
