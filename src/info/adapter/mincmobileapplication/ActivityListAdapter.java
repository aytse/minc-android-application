package info.adapter.mincmobileapplication;

import info.internal.mincmobileapplication.ClickableParser;
import info.internal.mincmobileapplication.ClickableTextView;
import info.internal.mincmobileapplication.ClickableTextView.ClickableWord;
import info.internal.mincmobileapplication.DownloadImagesTask;
import info.main.mincmobileapplication.R;
import info.model.mincmobileapplication.ActivityItem;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityListAdapter extends BaseAdapter{
	ArrayList<ActivityItem>activities;
	Activity a;
	
	public ActivityListAdapter() {
		activities = null;
		a = null;
	}
	
	public ActivityListAdapter(Activity a, ArrayList<ActivityItem>activities) {
		this.activities = activities;
		this.a = a;
	}
	
	@Override
	public int getCount() {
		return activities.size();
	}

	@Override
	public ActivityItem getItem(int position) {
		return activities.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderActivity holder;

		if (convertView == null) {
			holder = new ViewHolderActivity();
			
			LayoutInflater inflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.activity_list_element, parent, false);
			
			holder.posterPic = (ImageView) convertView.findViewById(R.id.people_profile_pic);
			holder.activity = (ClickableTextView) convertView.findViewById(R.id.status);
			holder.time = (TextView) convertView.findViewById(R.id.time);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolderActivity) convertView.getTag();
		}
		// SET ELEMENTs OF LISTVIEW
		// PARSE TEXTVIEW TO RETURN 3 POSSIBLE LINKS
		
		ActivityItem currentItem = activities.get(position);
		
		holder.time.setText(currentItem.getTime());
		holder.posterPic.setTag(currentItem.getIcon());
		new DownloadImagesTask(a).execute(holder.posterPic);
		
		String currentActivity = currentItem.getActivity();
		
		ClickableParser cp = new ClickableParser();
		
		ArrayList<ClickableWord> clickableWords = cp.toParse(a, currentActivity);
		
		holder.activity.setTextWithClickableWords(cp.getString(), clickableWords);
		/**
		 * NEXT CHANGE TEXT WITHOUT SPLITTER
		 * THEN SET TEXT WITH NEW STRING
		 * THEN CALL SETTEXTWITHCLICKABLEWORDS
		 */
		
		return convertView;
	}

}

class ViewHolderActivity {
	ImageView posterPic;
	ClickableTextView activity;
	TextView time;
}
