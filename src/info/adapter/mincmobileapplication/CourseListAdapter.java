package info.adapter.mincmobileapplication;

import info.internal.mincmobileapplication.DownloadImagesTask;
import info.main.mincmobileapplication.R;
import info.model.mincmobileapplication.ElementItem;
import info.model.mincmobileapplication.FolderItem;
import info.server.mincmobileapplication.ConnectionFunctions;
import info.server.mincmobileapplication.ConnectionTask;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class CourseListAdapter extends BaseAdapter {
	private ArrayList<ElementItem>courses;
	private String type;
	private Activity a;

	JSONArray jArr;
	ArrayList<String> collectionNames;
	ArrayList<FolderItem> collections;
	
	private ArrayList<ElementItem>copy_courses;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	public CourseListAdapter() {
		courses = null;
		a = null;
		userSettings = a.getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
	}
	
	public void filter(String charText) {
		if(copy_courses==null) {
			copy_courses = new ArrayList<ElementItem>();
			copy_courses.addAll(courses);
		}
		charText = charText.toLowerCase(Locale.getDefault());
		courses.clear();
		if (charText.length() == 0) {
			courses.addAll(copy_courses);
		}
		else {
			for(int j = 0; j < copy_courses.size(); j++) {
				if(copy_courses.get(j).getName().toLowerCase(Locale.getDefault()).contains(charText)) {
					courses.add(copy_courses.get(j));
				}
			}
		}
		notifyDataSetChanged();
	}
	
	public CourseListAdapter(Activity a, String type, ArrayList<ElementItem>courses) {
		this.courses = courses;
		this.type = type;
		this.a = a;
		userSettings = a.getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
	}
	
	@Override
	public int getCount() {
		return courses.size();
	}

	@Override
	public Object getItem(int position) {
		return courses.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolderCourses holder;
		if (convertView == null) {
			holder = new ViewHolderCourses();
			
			LayoutInflater inflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.course_list_element, parent, false);
		
			holder.coursePic = (ImageView) convertView.findViewById(R.id.course_pic);
			holder.courseName = (TextView) convertView.findViewById(R.id.course_name);
			holder.collectButton = (Button) convertView.findViewById(R.id.collect_button);
			holder.connectButton = (Button) convertView.findViewById(R.id.connect_button);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolderCourses) convertView.getTag();
		}
		
		holder.collectButton.setTag(position);
		
		holder.courseName.setText(courses.get(position).getName());
		holder.courseName.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String currentElement = courses.get((Integer) holder.collectButton.getTag()).getId();
				if(getType().equals("user")) {
					userSettingsEditor.putString(ConnectionFunctions.SELECTEDID, currentElement);
					userSettingsEditor.commit();
					String openUserCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserOtherInfo_userid.php?userid="+
							userSettings.getString(ConnectionFunctions.SELECTEDID, null);
					new ConnectionTask(a,"open_user").execute(openUserCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Loading user...", true, false);
				}
				else if(getType().equals("course")){
					userSettingsEditor.putString(ConnectionFunctions.COURSEID, currentElement);
					userSettingsEditor.commit();
					String openCourseCall = ConnectionFunctions.FUNCTIONBASEURL+"getCourseInfo_courseid_userid.php?courseid="+userSettings.getString(ConnectionFunctions.COURSEID, null)+
							"&userid="+userSettings.getString(ConnectionFunctions.USERID, null);
					new ConnectionTask(a, "open_course").execute(openCourseCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Loading course...", true, false);
				}
				else if(getType().equals("document")) {
					userSettingsEditor.putString(ConnectionFunctions.DOCUMENTID, currentElement);
					userSettingsEditor.commit();
					String openCourseCall = ConnectionFunctions.FUNCTIONBASEURL+"getDocumentURL_rid.php?rid="+userSettings.getString(ConnectionFunctions.DOCUMENTID, null);
					new ConnectionTask(a, "open_document").execute(openCourseCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Loading document...", true, false);
				}
				else if(getType().equals("question")) {
					userSettingsEditor.putString(ConnectionFunctions.QUESTIONID, currentElement);
					userSettingsEditor.commit();
					String openCourseCall = ConnectionFunctions.FUNCTIONBASEURL+"getQuestion_questionid_userid.php?questionidid="+userSettings.getString(ConnectionFunctions.QUESTIONID, null)+
							"&userid="+userSettings.getString(ConnectionFunctions.USERID, null);
					new ConnectionTask(a, "open_question").execute(openCourseCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Loading question...", true, false);
				}
				else if(getType().equals("group")) {
					userSettingsEditor.putString(ConnectionFunctions.GROUPID, currentElement);
					userSettingsEditor.commit();
					String openGroupCall = ConnectionFunctions.FUNCTIONBASEURL+"getGroupInfo_groupid_userid.php?groupid="+userSettings.getString(ConnectionFunctions.GROUPID, null)+
							"&userid="+userSettings.getString(ConnectionFunctions.USERID, null);
					new ConnectionTask(a, "open_group").execute(openGroupCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Loading group...", true, false);
				}
			}
			
		});
		
		holder.coursePic.setTag(courses.get(position).getAvatar());
		new DownloadImagesTask(a).execute(holder.coursePic);
		
		holder.collectButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int currentIndex = (Integer) holder.collectButton.getTag();
				final String currentId=courses.get(currentIndex).getId();	
				
				for(int i=0; i<collections.size(); i++) {
					String c=collections.get(i).getName();
					collectionNames.add(c);
				}
				
				String[] collectionArray = new String[collections.size()];
				collectionNames.toArray(collectionArray);
				
				AlertDialog.Builder builder = new AlertDialog.Builder(a);
				builder.setTitle("Add to collection")
				.setItems(collectionArray, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String collectionId = collections.get(which).getId();
						String collectionURL = ConnectionFunctions.FUNCTIONBASEURL+"addToCollection_userid_collectionid_itemid.php?userid="+
						userSettings.getString(ConnectionFunctions.USERID, null)+"&collectionid="+collectionId+"&itemid="+currentId;
						new ConnectionTask(a, "add_collection").execute(collectionURL);
						ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Loading...", true, false);
					}
					
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		
		});
		
		if(getType().equals("user")) {
			holder.connectButton.setVisibility(View.VISIBLE);
			holder.connectButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					final String getCurrentElement = courses.get((Integer) holder.collectButton.getTag()).getId();
					String getCurrentName = courses.get((Integer) holder.collectButton.getTag()).getName();
					AlertDialog.Builder builder = new AlertDialog.Builder(a);
					builder.setTitle("Add a new connection")
					.setMessage("Add " + getCurrentName + " as your connection?  Please add some instruction text below.");
					final EditText input = new EditText(a);
					builder.setView(input);
					
					builder.setPositiveButton("Add Connection", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							String message = input.getText().toString();
							String sendConnectionRequestURL = ConnectionFunctions.FUNCTIONBASEURL+"sendConnectionRequest_fromUserid_toUserid_msg.php?fromUserid="+
							userSettings.getString("USERID", null)+"&toUserid="+getCurrentElement+"&msg="+message;
							new ConnectionTask(a, "connect_request").execute(sendConnectionRequestURL);
							ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Sending connection request...", true, false);
						}
						
					});
					
					builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// Cancelled
						}
						
					});
					
					AlertDialog dialog = builder.create();
					dialog.show();
				}
				
			});
		}
		return convertView;
	}
	
	public void setType(String type) {	
		this.type=type;
	}
	
	public String getType() {
		return type;
	}
	
	class ViewHolderCourses {
		ImageView coursePic;
		TextView courseName;
		Button collectButton;
		Button connectButton;
	}

}
