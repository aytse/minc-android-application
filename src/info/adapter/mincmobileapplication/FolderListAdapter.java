package info.adapter.mincmobileapplication;

import info.internal.mincmobileapplication.DownloadImagesTask;
import info.main.mincmobileapplication.CourseFolderFragment;
import info.main.mincmobileapplication.DescriptionFragment;
import info.main.mincmobileapplication.MainActivity;
import info.main.mincmobileapplication.R;
import info.model.mincmobileapplication.FolderItem;
import info.server.mincmobileapplication.ConnectionFunctions;
import info.server.mincmobileapplication.ConnectionTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FolderListAdapter extends BaseAdapter{
	ArrayList<FolderItem>folders;
	Activity a;

	JSONArray jArr;
	ArrayList<String> collectionNames;
	ArrayList<FolderItem> collections;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	public FolderListAdapter() {}
	
	public FolderListAdapter(Activity a, ArrayList<FolderItem>folders) {
		this.a = a;
		this.folders = folders;
		userSettings = a.getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		collections = new ArrayList<FolderItem>();
		collectionNames = new ArrayList<String>();
		String getCollectionsCall = ConnectionFunctions.FUNCTIONBASEURL+"getCollections_userid.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null);
		new CollectionTask("load_collections").execute(getCollectionsCall);
	}
	
	@Override
	public int getCount() {
		return folders.size();
	}

	@Override
	public Object getItem(int position) {
		return folders.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolderFolder holder;
		
		if (convertView == null) {
			holder = new ViewHolderFolder();
			
			LayoutInflater inflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.course_list_element, parent, false);
		
			holder.documentPic = (ImageView) convertView.findViewById(R.id.course_pic);
			holder.documentName = (TextView) convertView.findViewById(R.id.course_name);
			holder.collectButton = (Button) convertView.findViewById(R.id.collect_button);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolderFolder) convertView.getTag();
		}
		
		holder.collectButton.setTag(position);
		holder.collectButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int currentIndex = (Integer) holder.collectButton.getTag();
				final String currentId=folders.get(currentIndex).getId();	
				
				for(int i=0; i<collections.size(); i++) {
					String c=collections.get(i).getName();
					collectionNames.add(c);
				}
				
				String[] collectionArray = new String[collections.size()];
				collectionNames.toArray(collectionArray);
				
				AlertDialog.Builder builder = new AlertDialog.Builder(a);
				builder.setTitle("Add to collection")
				.setItems(collectionArray, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String collectionId = collections.get(which).getId();
						String collectionURL = ConnectionFunctions.FUNCTIONBASEURL+"addToCollection_userid_collectionid_itemid.php?userid="+
						userSettings.getString(ConnectionFunctions.USERID, null)+"&collectionid="+collectionId+"&itemid="+currentId;
						new ConnectionTask(a, "add_collection").execute(collectionURL);
						ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Loading...", true, false);
					}
					
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		
		});
		
		holder.documentName.setText(folders.get(position).getName());
		holder.documentName.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int currentIndex = (Integer) holder.collectButton.getTag();
				String currentUrl=folders.get(currentIndex).getUrl();
				String currentPic;
				if(folders.get(currentIndex).getIcon()==null)
					currentPic = "default picture";
				else
					currentPic = folders.get(currentIndex).getIcon();
				String currentDescription = folders.get(currentIndex).getDescription();
				if(currentPic.equals("https://hive.asu.edu/minc/images/nsdlThumb.png")) {
					String[] parts = currentUrl.split("https://hive.asu.edu/minc/");
					String urlLink = parts[1];
					try {
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlLink));
						a.startActivity(intent);
					} catch (ActivityNotFoundException e) {
						Toast.makeText(a, "No application can handle this request", Toast.LENGTH_LONG).show();
						e.printStackTrace();
					}
				}
				else if(!currentDescription.equals("null")) {
					userSettingsEditor.putString(ConnectionFunctions.DOCUMENTNAME, folders.get(currentIndex).getName());
					userSettingsEditor.putString(ConnectionFunctions.DESCRIPTION, currentDescription);
					userSettingsEditor.commit();
					((MainActivity) a).changeFragment(new DescriptionFragment(), userSettings.getString(ConnectionFunctions.FOLDERNAME, null));
				}
				else {
					if(currentUrl.contains("albumid=")) {
						String[] parts = currentUrl.split("albumid=");
						String second = parts[1];
						String[] parts2 = second.split("&");
						String collectionId = parts2[0];
						userSettingsEditor.putString(ConnectionFunctions.FOLDERID, collectionId);
						userSettingsEditor.putString(ConnectionFunctions.FOLDERNAME, folders.get((Integer) holder.collectButton.getTag()).getName());
						userSettingsEditor.commit();
						((MainActivity) a).changeFragment(new CourseFolderFragment(), userSettings.getString(ConnectionFunctions.FOLDERNAME, null));
					}
					else if(currentUrl.contains("questionid=")) {
						ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Loading question...", true, false);
						String[] parts = currentUrl.split("questionid=");
						String questionId = parts[1];
						userSettingsEditor.putString(ConnectionFunctions.QUESTIONID, questionId);
						userSettingsEditor.commit();
						String openQuestionCall = ConnectionFunctions.FUNCTIONBASEURL+"getQuestion_questionid_userid.php?questionid="+
						userSettings.getString(ConnectionFunctions.QUESTIONID, null)+"&userid="+userSettings.getString(ConnectionFunctions.USERID, null);
						new ConnectionTask(a, "open_question").execute(openQuestionCall);
					}
					else if(currentUrl.contains("userid=")) {
						ConnectionFunctions.pdLoadFeed = ProgressDialog.show(a, "", "Loading user...", true, false);
						String[] parts = currentUrl.split("userid=");
						String userId = parts[1];
						userSettingsEditor.putString(ConnectionFunctions.SELECTEDID, userId);
						userSettingsEditor.commit();
						String openUserCall =ConnectionFunctions.FUNCTIONBASEURL+"getUserOtherInfo_userid.php?userid="+userSettings.getString(ConnectionFunctions.SELECTEDID, null);
						new ConnectionTask(a, "open_user").execute(openUserCall);
					}
					else if(currentUrl.contains("https://hive.asu.edu/minc/images/photos/")) {
						String[] parts = currentUrl.split("https://hive.asu.edu/minc/images/photos/");
						String second = parts[1];
						parts = second.split("/");
						String collectionId = parts[1];
						userSettingsEditor.putString(ConnectionFunctions.FOLDERID, collectionId);
						userSettingsEditor.putString(ConnectionFunctions.FOLDERNAME, folders.get((Integer) holder.collectButton.getTag()).getName());
						userSettingsEditor.commit();
						((MainActivity) a).changeFragment(new CourseFolderFragment(), userSettings.getString(ConnectionFunctions.FOLDERNAME, null));
					}
					else {
						try {
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(currentUrl));
							a.startActivity(intent);
						} catch (ActivityNotFoundException e) {
							Toast.makeText(a, "No application can handle this request", Toast.LENGTH_LONG).show();
							e.printStackTrace();
						}
					}
				}
				
			}
			
		});
		
		if((folders.get(position).getIcon()==null))
				holder.documentPic.setImageResource(R.drawable.folder_icon);
		else {
			holder.documentPic.setTag(folders.get(position).getIcon());
			new DownloadImagesTask(a).execute(holder.documentPic);
		}
		return convertView;
	}
	
	private void postCheck(String task, String result) {
		try {
			if(task.equals("load_collections")) {
				jArr = new JSONArray(result);
				
				for(int i=0; i<jArr.length(); i++) {
					FolderItem c = new FolderItem();
					// NEED A GET COLLECTION FUNCTION
					c.setId(jArr.getJSONObject(i).getString("id"));
					c.setName(jArr.getJSONObject(i).getString("name"));
					c.setUrl(jArr.getJSONObject(i).getString("path"));
					c.setDescription("null");
					collections.add(c);
					
				}
				
				
				ConnectionFunctions.pdLoadFeed.dismiss();
			}		
		} catch (JSONException e) {
			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	private class CollectionTask extends AsyncTask<String, View, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public CollectionTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = a.getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}	
	}

}

class ViewHolderFolder {
	TextView documentName;
	ImageView documentPic;
	Button collectButton;
}
