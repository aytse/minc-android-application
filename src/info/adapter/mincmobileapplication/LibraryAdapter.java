package info.adapter.mincmobileapplication;

import info.main.mincmobileapplication.R;
import info.model.mincmobileapplication.ElementItem;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LibraryAdapter extends BaseAdapter{
	ArrayList<ElementItem>folders;
	Activity a;
	
	public LibraryAdapter() {
		folders = null;
		a = null;
	}
	
	public LibraryAdapter(Activity a, ArrayList<ElementItem>folders) {
		this.folders = folders;
		this.a = a;
	}
	
	@Override
	public int getCount() {
		return folders.size();
	}

	@Override
	public Object getItem(int position) {
		return folders.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderResource holder;
		
		if (convertView == null) {
			holder = new ViewHolderResource();
			
			LayoutInflater inflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.course_library_element, parent, false);
			
			holder.folderName = (TextView) convertView.findViewById(R.id.folder_name);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolderResource) convertView.getTag();
		}
		
		holder.folderName.setText(folders.get(position).getName());
		
		return convertView;
	}
	
}

class ViewHolderResource {
	TextView folderName;
}