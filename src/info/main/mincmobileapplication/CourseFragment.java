package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.ActivityListAdapter;
import info.internal.mincmobileapplication.DownloadImagesTask;
import info.internal.mincmobileapplication.StringFormatter;
import info.model.mincmobileapplication.ActivityItem;
import info.model.mincmobileapplication.AnnouncementItem;
import info.server.mincmobileapplication.ConnectionFunctions;
import info.server.mincmobileapplication.ConnectionTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class CourseFragment extends Fragment{
	LinearLayout courseInformation;
	ImageView coursePic;
	TextView courseName, courseYear, courseTime,
	myRating, avgRating, numberRate;
	EditText postQuestionInput;
	Button postQuestionButton, rateCourseButton;
	LinearLayout announcementsLayout;
	TextView announcement1, announcement2;
	
	JSONArray jArr;
	ArrayList<ActivityItem>activities;
	
	// WILL BE CALLED IN COURSEANNOUNCMENT FRAGMENT
	public static ArrayList<AnnouncementItem>announcements;
	
	ActivityListAdapter adapter;
	
	ListView course_activities_list;
	
	int activityCount=0;
	int newActivityCount=9;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	public CourseFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.course_group_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		courseInformation = (LinearLayout) rootView.findViewById(R.id.course_information);
		courseInformation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((MainActivity) getActivity()).changeFragment(new CourseInformationFragment(), userSettings.getString(ConnectionFunctions.COURSENAME, null));
			}
			
		});
		
		coursePic = (ImageView) rootView.findViewById(R.id.course_pic_large);
		coursePic.setTag(userSettings.getString(ConnectionFunctions.COURSEAVATAR, null));
		new DownloadImagesTask(getActivity()).execute(coursePic);
		
		rateCourseButton = (Button) rootView.findViewById(R.id.rate_course_button);
		rateCourseButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle("Rating course")
				.setItems(ConnectionFunctions.ratings, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int rateposition) {
						String rateResourceCall = ConnectionFunctions.FUNCTIONBASEURL+"setResourceRating_userid_resrcid_rating.php?userid="+
					userSettings.getString(ConnectionFunctions.USERID, null)+"&resrcid="+userSettings.getString(ConnectionFunctions.COURSEID, null)+"&rating="+ConnectionFunctions.ratings[rateposition];
						new ListViewTask("rate_course").execute(rateResourceCall);
						ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
					}
					
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
			
		});
		
		courseName = (TextView) rootView.findViewById(R.id.course_name);
		courseName.setText(userSettings.getString(ConnectionFunctions.COURSENAME, null));
		courseYear = (TextView) rootView.findViewById(R.id.year_term);
		courseYear.setText(userSettings.getString(ConnectionFunctions.COURSETERM, null)+" "+
				userSettings.getString(ConnectionFunctions.COURSEYEAR, null));
		courseTime = (TextView) rootView.findViewById(R.id.course_time);
		courseTime.setText(userSettings.getString(ConnectionFunctions.COURSESTART, null));
		
		if(userSettings.getString(ConnectionFunctions.COURSEAVGRATING, null).equals("null"))
			userSettingsEditor.putString(ConnectionFunctions.COURSEAVGRATING, "");
		if(userSettings.getString(ConnectionFunctions.COURSEMYRATING, null).equals("null"))
			userSettingsEditor.putString(ConnectionFunctions.COURSEMYRATING, "");
		if(userSettings.getString(ConnectionFunctions.COURSENUMBERRATE, null).equals("null"))
			userSettingsEditor.putString(ConnectionFunctions.COURSENUMBERRATE, "0");
		userSettingsEditor.commit();
		
		myRating = (TextView) rootView.findViewById(R.id.my_course_rating_info);
		myRating.setText(userSettings.getString(ConnectionFunctions.COURSEMYRATING, null)+"/5");
		avgRating = (TextView) rootView.findViewById(R.id.course_rating_info);
		avgRating.setText(userSettings.getString(ConnectionFunctions.COURSEAVGRATING, null)+"/5");
		numberRate = (TextView) rootView.findViewById(R.id.course_number_rate_info);
		numberRate.setText("("+userSettings.getString(ConnectionFunctions.COURSENUMBERRATE, null)+" raters)");		
		
		postQuestionInput = (EditText) rootView.findViewById(R.id.postQuestionInput);
		
		postQuestionButton = (Button) rootView.findViewById(R.id.postQuestionButton);
		postQuestionButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(!postQuestionInput.getText().toString().equals("")) {
					String question = postQuestionInput.getText().toString().replaceAll(" ", "%20");
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Posting question...", true, false);
					String setQuestionCall = ConnectionFunctions.FUNCTIONBASEURL+"setQuestion_userid_target_content.php?userid="+
							userSettings.getString(ConnectionFunctions.USERID, null)+"&target="+userSettings.getString(ConnectionFunctions.COURSEID, null)+
							"&content="+question;
					new ConnectionTask(getActivity(), "post_activity").execute(setQuestionCall);
				}
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(postQuestionInput.getWindowToken(), 0);
			}
			
		});
		
		activities = new ArrayList<ActivityItem>();
		announcements = new ArrayList<AnnouncementItem>();
		
		// ADD FIRST TWO ANNOUNCEMENTS
		announcementsLayout = (LinearLayout) rootView.findViewById(R.id.announcementsLayout);
		announcement1 = (TextView) rootView.findViewById(R.id.announcement1);
		announcement2 = (TextView) rootView.findViewById(R.id.announcement2);
		
		course_activities_list = (ListView) rootView.findViewById(R.id.activities_list);
		
		String getQuestionCall = ConnectionFunctions.FUNCTIONBASEURL+"getCourseQuestions_courseid_from_to.php?courseid="+userSettings.getString(ConnectionFunctions.COURSEID, null)+"&from=0&to=9";
		String getAnnouncementsCall = ConnectionFunctions.FUNCTIONBASEURL+"getCourseAnnouncement_courseid.php?courseid="+userSettings.getString(ConnectionFunctions.COURSEID, null);
		new ListViewTask("load_activity").execute(getQuestionCall);
		new ListViewTask("load_announcements").execute(getAnnouncementsCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading activities...", true, false); 
		
		announcementsLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((MainActivity) getActivity()).changeFragment(new CourseAnnouncementFragment(), userSettings.getString(ConnectionFunctions.COURSENAME, null));
			}
			
		});
		
		course_activities_list.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScroll(AbsListView view, int firstVisible, int visibleCount, int totalCount) {
				
			}

			@Override
			public void onScrollStateChanged(AbsListView listView, int scrollState) {
				if (scrollState == SCROLL_STATE_IDLE) {
					if(listView.getLastVisiblePosition() >= listView.getCount() - 1) {
						activityCount = newActivityCount;
						newActivityCount = newActivityCount + 10;
						String updateActivityCall= ConnectionFunctions.FUNCTIONBASEURL + "getUserActivity_userid_from_to.php?userid="+userSettings.getString(ConnectionFunctions.SELECTEDID, null)+"&from="+activityCount+"&to="+newActivityCount;
						new ListViewTask("load_activity").execute(updateActivityCall);
						ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading activities...", true, false);
						
					}
				}
			}
			
		});
		
		adapter = new ActivityListAdapter(getActivity(), activities);
		course_activities_list.setAdapter(adapter);
		
		return rootView;
	}
	
	private void postCheck(String task, String result) {
		try {
			// Returns first 10 activities
			if(task.equals("load_activity")) {
			    jArr = new JSONArray(result);
			    StringFormatter s = new StringFormatter();
			    
				for(int i=0; i<jArr.length();i++) {
					//JSONArray jArr2 = jArr.getJSONArray(i);
					ActivityItem a = new ActivityItem();
					a.setId(jArr.getJSONObject(i).getString("id"));
					a.setType(jArr.getJSONObject(i).getString("app"));
					a.setIcon(jArr.getJSONObject(i).getString("avatar"));
					String activityFormat = s.format(jArr.getJSONObject(i).getString("title"));
					a.setActivity(activityFormat);
					a.setTime(jArr.getJSONObject(i).getString("created"));
					a.setCreatorLink(jArr.getJSONObject(i).getString("actor"));
					a.setPostLink(jArr.getJSONObject(i).getString("cid"));
					a.setTargetLink(jArr.getJSONObject(i).getString("target"));
					activities.add(a);
				}
				adapter.notifyDataSetChanged();
			}
			else if(task.equals("load_announcements")) {
				jArr = new JSONArray(result);
				
				for(int i = 0; i < jArr.length(); i++) {
					AnnouncementItem a = new AnnouncementItem();
					a.setId(jArr.getJSONObject(i).getString("anounceid"));
					a.setTitle(jArr.getJSONObject(i).getString("title"));
					a.setMessage(jArr.getJSONObject(i).getString("message"));
					a.setDate(jArr.getJSONObject(i).getString("date"));
					announcements.add(a);
				}
				
				announcement1.setText(announcements.get(0).getTitle());
				announcement2.setText(announcements.get(1).getTitle());
				
			}
			else if (task.equals("update_course")) {
				ConnectionFunctions.pdLoadFeed.dismiss();
				JSONObject jObj = new JSONArray(result).getJSONObject(0);
		
				userSettingsEditor.putString(ConnectionFunctions.COURSEMYRATING, jObj.getString("myrating"));
				userSettingsEditor.putString(ConnectionFunctions.COURSEAVGRATING, jObj.getString("avgrating"));
				userSettingsEditor.putString(ConnectionFunctions.COURSENUMBERRATE, jObj.getString("totcount"));				
				userSettingsEditor.commit();
				
				if(userSettings.getString(ConnectionFunctions.COURSEAVGRATING, null).equals("null"))
					userSettingsEditor.putString(ConnectionFunctions.COURSEAVGRATING, "");
				if(userSettings.getString(ConnectionFunctions.COURSEMYRATING, null).equals("null"))
					userSettingsEditor.putString(ConnectionFunctions.COURSEMYRATING, "");
				if(userSettings.getString(ConnectionFunctions.COURSENUMBERRATE, null).equals("null"))
					userSettingsEditor.putString(ConnectionFunctions.COURSENUMBERRATE, "0");
				userSettingsEditor.commit();
				
				myRating.setText(userSettings.getString(ConnectionFunctions.COURSEMYRATING, null)+"/5");
				avgRating.setText(userSettings.getString(ConnectionFunctions.COURSEAVGRATING, null)+"/5");
				numberRate.setText("("+userSettings.getString(ConnectionFunctions.COURSENUMBERRATE, null)+" raters)");
			}
			else if(task.equals("rate_course")) {
				ConnectionFunctions.pdLoadFeed.dismiss();
				new ListViewTask("update_course").execute(ConnectionFunctions.FUNCTIONBASEURL+"getCourseInfo_courseid_userid.php?courseid="+
				userSettings.getString(ConnectionFunctions.COURSEID, null)+"&userid="+userSettings.getString(ConnectionFunctions.USERID, null));
			}
		} catch (JSONException e) {
			if(task.equals("load_announcements")) {
				announcementsLayout.setVisibility(View.GONE);
				announcement1.setVisibility(View.GONE);
				announcement2.setVisibility(View.GONE);
			}
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}

	// Have to re call Connection Task from within to save lists, adapter
		// and Listview content.  Cannot figure out a way to get the content
		// from ConenctionFunction.  Will check later.
		private class ListViewTask extends AsyncTask<String, Void, String> {
			String task = "";
			StringBuilder sb;
			SSLContext context;
			
			public ListViewTask(String t) {
				task = t;
				sb = new StringBuilder();
				
				try {
					CertificateFactory cf = CertificateFactory.getInstance("X.509");
					InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
					java.security.cert.Certificate ca;
					try {
						ca = cf.generateCertificate(caInput);
						System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
					} finally {
						caInput.close();
					}
				
					//Create a KeyStore containing our trusted CAs
					String keyStoreType = KeyStore.getDefaultType();
					KeyStore keyStore = KeyStore.getInstance(keyStoreType);
					keyStore.load(null, null);
					keyStore.setCertificateEntry("ca", ca);
				
					//Create a TrustManager that trusts the CAs in our KeyStore
					String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
					TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
					tmf.init(keyStore);
				
					//Create an SSLContext that uses our TrustManager
					context = SSLContext.getInstance("TLS");
					context.init(null, tmf.getTrustManagers(), null);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
			}

			@Override
			protected String doInBackground(String... params) {
				try {
					URL url = new URL(params[0]);
					HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
					urlConnection.setSSLSocketFactory(context.getSocketFactory());
					InputStream inStream = urlConnection.getInputStream();
					
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
					String line = "";
					while ((line = bufferedReader.readLine()) != null) {
						if(line != null) {
							sb.append(line + "/n");
						}
					}
					bufferedReader.close();
					inStream.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				return sb.toString();
			}

			@Override
			protected void onPostExecute(String result) {
				postCheck(task, result);
				super.onPostExecute(result);
			}	
		}
}
