package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.AnnouncementListAdapter;
import info.server.mincmobileapplication.ConnectionFunctions;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class CourseAnnouncementFragment extends Fragment{
	TextView bulletinTitle;
	ListView courseAnnouncements;
	
	AnnouncementListAdapter bulletinAdapter;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.course_announcement_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		bulletinTitle = (TextView) rootView.findViewById(R.id.bulletin_title);
		bulletinTitle.setText("Bulletins from "+userSettings.getString(ConnectionFunctions.COURSENAME, null));
		
		courseAnnouncements = (ListView) rootView.findViewById(R.id.course_announcement_list);
		courseAnnouncements.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				userSettingsEditor.putString(ConnectionFunctions.ANNOUNCEMENTTITLE,CourseFragment.announcements.get(position).getTitle());
				userSettingsEditor.putString(ConnectionFunctions.ANNOUNCEMENTMESSAGE, CourseFragment.announcements.get(position).getMessage());
				userSettingsEditor.commit();
				if(userSettings.getString(ConnectionFunctions.ANNOUNCEMENTMESSAGE, null).equals(null)) {}
				else {
					((MainActivity) getActivity()).changeFragment(new AnnouncementFragment(), userSettings.getString(ConnectionFunctions.COURSENAME, null));
				}
			}
		});
		
		bulletinAdapter = new AnnouncementListAdapter(getActivity(), CourseFragment.announcements);
		courseAnnouncements.setAdapter(bulletinAdapter);
		
		return rootView;
	}
	
}
