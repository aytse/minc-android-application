package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.CourseListAdapter;
import info.model.mincmobileapplication.ElementItem;
import info.server.mincmobileapplication.ConnectionFunctions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class ClassFragment extends Fragment{

	EditText searchCourseInput;
	Button searchCourseButton;
	public static ListView courseList;
	public static ListView myCourseList;
	
	JSONArray jArr, all_jArr;
	ArrayList<ElementItem> courses, allCourses;
	CourseListAdapter cAdapter, cAllAdapter;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	public ClassFragment() {}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.class_fragment, container, false);
	
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		searchCourseInput = (EditText) rootView.findViewById(R.id.searchCourseInput);
		
		searchCourseButton = (Button) rootView.findViewById(R.id.searchCourseButton);
		searchCourseButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String text = searchCourseInput.getText().toString().toLowerCase(Locale.getDefault());
				cAllAdapter.filter(text);
				
			}
			
		});

		myCourseList = (ListView) rootView.findViewById(R.id.my_course_list);
		courseList = (ListView) rootView.findViewById(R.id.course_list);
		
		String getCourseCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserCourselist_userid.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null);
		String getAllCourseCall = ConnectionFunctions.FUNCTIONBASEURL+"getAllCourselist.php";
		new CourseListTask("load_course").execute(getCourseCall);
		new CourseListTask("load_all_courses").execute(getAllCourseCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading courses...", true, false);
		
		return rootView;
	}
	
	private void postCheck(String task, String result) {
		try {
			if (task.equals("load_course")) {
				jArr = new JSONArray(result);
				
				courses = new ArrayList<ElementItem>();

				for (int i = 0; i < jArr.length(); i++) {
					ElementItem c = new ElementItem();
					c.setId(jArr.getJSONObject(i).getString("id"));
					c.setName(jArr.getJSONObject(i).getString("name"));
					c.setAvatar(jArr.getJSONObject(i).getString("avatar"));
					c.setType("course");
					courses.add(c);
				}
				if (cAdapter == null) {
					cAdapter = new CourseListAdapter(getActivity(), "course", courses);
					ClassFragment.myCourseList.setAdapter(cAdapter);
				} else
					cAdapter.notifyDataSetChanged();

				ConnectionFunctions.pdLoadFeed.dismiss();
			}

			else if (task.equals("load_all_courses")) {
				all_jArr = new JSONArray(result);
				
				allCourses = new ArrayList<ElementItem>();

				for (int i = 0; i < all_jArr.length(); i++) {
					ElementItem allC = new ElementItem();
					allC.setId(all_jArr.getJSONObject(i).getString("id"));
					allC.setName(all_jArr.getJSONObject(i).getString("name"));
					allC.setAvatar(all_jArr.getJSONObject(i)
							.getString("avatar"));
					allC.setType("course");
					allCourses.add(allC);
				}
				if (cAllAdapter == null) {
					cAllAdapter = new CourseListAdapter(getActivity(), "course", allCourses);
					ClassFragment.courseList.setAdapter(cAllAdapter);
				} else
					cAllAdapter.notifyDataSetChanged();

				ConnectionFunctions.pdLoadFeed.dismiss();
			}
			
		} catch (JSONException e) {
			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	private class CourseListTask extends AsyncTask<String, Void, String> {
		String task  = "";
		StringBuilder sb;
		SSLContext context;
		
		public CourseListTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}
		
		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}
	}
	
}
