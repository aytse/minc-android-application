package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.LibraryAdapter;
import info.internal.mincmobileapplication.DownloadImagesTask;
import info.model.mincmobileapplication.ElementItem;
import info.server.mincmobileapplication.ConnectionFunctions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class GroupInformationFragment extends Fragment{
	ScrollView course_layout, group_layout;
	
	TextView name, created, creator, description;
	
	RelativeLayout group_members;
	
	ImageView member_pic1, member_pic2, member_pic3, member_pic4;
	TextView member_name1, member_name2, member_name3, member_name4;
	
	ListView group_library;
	
	JSONArray jArr, member_jArr, admin_jArr;
	
	public static ArrayList<ElementItem> members;
	public static ArrayList<ElementItem> admins;
	
	public static ArrayList<ElementItem> folders;
	
	LibraryAdapter adapter;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.course_group_information_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		course_layout = (ScrollView) rootView.findViewById(R.id.course_information_layout);
		course_layout.setVisibility(View.GONE);
		
		group_layout = (ScrollView) rootView.findViewById(R.id.group_information_layout);
		group_layout.setVisibility(View.VISIBLE);
		
		name = (TextView) rootView.findViewById(R.id.name_info);
		name.setText(userSettings.getString(ConnectionFunctions.GROUPNAME, null));
		created = (TextView) rootView.findViewById(R.id.created_info);
		created.setText(userSettings.getString(ConnectionFunctions.GROUPCREATED, null));
		creator = (TextView) rootView.findViewById(R.id.creator_info);
		creator.setText(userSettings.getString(ConnectionFunctions.GROUPCREATOR, null));
		description = (TextView) rootView.findViewById(R.id.group_description_info);
		description.setText(userSettings.getString(ConnectionFunctions.GROUPDESCRIPTION, null));
		
		members = new ArrayList<ElementItem>();
		admins = new ArrayList<ElementItem>();
		
		folders = new ArrayList<ElementItem>();
		
		String getMembersCall = ConnectionFunctions.FUNCTIONBASEURL+"getGroupMembers_groupid.php?groupid="+userSettings.getString(ConnectionFunctions.GROUPID, null);
		String getGroupLibraryCall = ConnectionFunctions.FUNCTIONBASEURL+"getGroupLibraryList_groupid.php?groupid="+userSettings.getString(ConnectionFunctions.GROUPID, null);
		new GroupInformationTask("load_members").execute(getMembersCall);
		new GroupInformationTask("load_group_library").execute(getGroupLibraryCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
		
		member_pic1 = (ImageView) rootView.findViewById(R.id.member_pic_1);
		member_name1 = (TextView) rootView.findViewById(R.id.member_name_1);
		member_pic2 = (ImageView) rootView.findViewById(R.id.member_pic_2);
		member_name2 = (TextView) rootView.findViewById(R.id.member_name_2);
		member_pic3 = (ImageView) rootView.findViewById(R.id.member_pic_3);
		member_name3 = (TextView) rootView.findViewById(R.id.member_name_3);
		member_pic4 = (ImageView) rootView.findViewById(R.id.member_pic_4);
		member_name4 = (TextView) rootView.findViewById(R.id.member_name_4);
		
		group_members = (RelativeLayout) rootView.findViewById(R.id.course_members);
		group_members.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((MainActivity) getActivity()).changeFragment(new GroupMemberFragment(), userSettings.getString(ConnectionFunctions.GROUPNAME, null));
			}
			
		});
		
		group_library = (ListView) rootView.findViewById(R.id.course_library_list);
		group_library.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				userSettingsEditor.putString(ConnectionFunctions.FOLDERID, folders.get(position).getId());
				userSettingsEditor.putString(ConnectionFunctions.FOLDERNAME, folders.get(position).getName());
				userSettingsEditor.commit();
				((MainActivity) getActivity()).changeFragment(new CourseFolderFragment(), userSettings.getString(ConnectionFunctions.FOLDERNAME, null));
			}
			
		});
		
		adapter = new LibraryAdapter(getActivity(), folders);
		group_library.setAdapter(adapter);
		
		return rootView;
	}

	private void postCheck(String task, String result) {
		try {
			if(task.equals("load_members")) {
				jArr = new JSONArray(result);
				
				for(int i=0; i<jArr.length(); i++) {
					if(jArr.getJSONObject(i).getString("type").equals("Member")) {
						member_jArr = new JSONArray(jArr.getJSONObject(i).getString("data"));
						for(int j=0; j<member_jArr.length();j++) {
							ElementItem e = new ElementItem();
							e.setId(member_jArr.getJSONObject(j).getString("userid"));
							e.setName(member_jArr.getJSONObject(j).getString("name"));
							//member_work.add(member_jArr.getJSONObject(j).getString(""));
							e.setAvatar(member_jArr.getJSONObject(j).getString("avatar"));
							members.add(e);
						}
						member_pic2.setTag(members.get(0).getAvatar());
						new DownloadImagesTask(getActivity()).execute(member_pic2);
						member_pic3.setTag(members.get(1).getAvatar());
						new DownloadImagesTask(getActivity()).execute(member_pic3);
						member_pic4.setTag(members.get(2).getAvatar());
						new DownloadImagesTask(getActivity()).execute(member_pic4);
						
						member_name2.setText(members.get(0).getName());
						member_name3.setText(members.get(1).getName());
						member_name4.setText(members.get(2).getName());
					}
					else if(jArr.getJSONObject(i).getString("type").equals("Administrator")) {
						admin_jArr = new JSONArray(jArr.getJSONObject(i).getString("data"));
						for(int j=0; j<admin_jArr.length();j++) {
							ElementItem e = new ElementItem();
							e.setId(admin_jArr.getJSONObject(j).getString("userid"));
							e.setName(admin_jArr.getJSONObject(j).getString("name"));
							//instructor_work.add(instructor_jArr.getJSONObject(j).getString(""));
							e.setAvatar(admin_jArr.getJSONObject(j).getString("avatar"));
							admins.add(e);
						}
						member_pic1.setTag(admins.get(0).getAvatar());
						new DownloadImagesTask(getActivity()).execute(member_pic1);
						
						member_name1.setText(admins.get(0).getName());
						
					}
				}
			}
			
			else if(task.equals("load_group_library")) {
				jArr = new JSONArray(result);
				
				for(int i = 0; i<jArr.length(); i++) {
					ElementItem e = new ElementItem();
					e.setId(jArr.getJSONObject(i).getString("id"));
					e.setName(jArr.getJSONObject(i).getString("name"));
					folders.add(e);
				}
				adapter.notifyDataSetChanged();
			}
			
		} catch (JSONException e) {
			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	private class GroupInformationTask extends AsyncTask<String, Void, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public GroupInformationTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}	
	}
}
