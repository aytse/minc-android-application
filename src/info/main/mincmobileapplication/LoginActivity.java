package info.main.mincmobileapplication;

import info.server.mincmobileapplication.ConnectionFunctions;
import info.server.mincmobileapplication.ConnectionTask;

import org.json.JSONArray;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {

	EditText editTextUsername, editTextPassword;
	Button btnLogin, btnSignUp;
	TextView loginErrorMsg;

	JSONArray jArr;
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		userSettings = getSharedPreferences("UserPreferences", MODE_PRIVATE);
		userSettingsEditor = userSettings.edit();

		loginErrorMsg = (TextView) findViewById(R.id.login_error);

		// Get XML elements
		editTextUsername = (EditText) findViewById(R.id.usernameInput);
		editTextPassword = (EditText) findViewById(R.id.passwordInput);

		btnLogin = (Button) findViewById(R.id.loginButton);
		btnLogin.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(editTextPassword.getWindowToken(),
						0);
				if (!editTextUsername.getText().toString().equals("")
						&& !editTextPassword.getText().toString().equals("")) {
					String loginCall = ConnectionFunctions.FUNCTIONBASEURL + "checklogin_username_password.php?username="
							+ editTextUsername.getText().toString()
							+ "&password="
							+ editTextPassword.getText().toString();
					new ConnectionTask(LoginActivity.this,"Login").execute(loginCall);
					
					String getLoginCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserOtherInfo_userid.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null);
					new ConnectionTask(LoginActivity.this, "open_me").execute(getLoginCall);
					
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(LoginActivity.this, "",
							"Logging in...", true, false);
					/*
					new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								Thread.sleep(6000);
								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										ConnectionFunctions.pdLoadFeed.dismiss();
										Toast.makeText(LoginActivity.this,
												"Error connecting to server",
												Toast.LENGTH_SHORT).show();
									}
								});
							} catch (InterruptedException e) {
								Thread.currentThread().interrupt();
							}
						}
						
					}).start();*/
				} else {
					Toast.makeText(LoginActivity.this,
							"Please enter a username and a password",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
