package info.main.mincmobileapplication;

import info.server.mincmobileapplication.ConnectionFunctions;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class AnnouncementFragment extends Fragment{
	TextView announcementTitle, message;
	ListView courseDocumentList;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.folder_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		courseDocumentList = (ListView) rootView.findViewById(R.id.course_document_list);
		courseDocumentList.setVisibility(8);
		
		announcementTitle = (TextView) rootView.findViewById(R.id.course_folder_title);
		announcementTitle.setText(userSettings.getString(ConnectionFunctions.ANNOUNCEMENTTITLE, null));
	
		message = (TextView) rootView.findViewById(R.id.description);
		message.setVisibility(0);
		message.setText(userSettings.getString(ConnectionFunctions.ANNOUNCEMENTMESSAGE, null));
		message.setMovementMethod(LinkMovementMethod.getInstance());
		
		return rootView;
	}
	
}
