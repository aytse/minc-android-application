package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.CourseListAdapter;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

public class GroupMemberFragment extends Fragment{
	LinearLayout groupAdminLayout, courseInstructorLayout, courseTaLayout;
	
	ListView admins, members;
	
	CourseListAdapter memberAdapter;
	CourseListAdapter adminAdapter;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.course_member_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		courseInstructorLayout = (LinearLayout) rootView.findViewById(R.id.course_instructor_layout);
		courseInstructorLayout.setVisibility(View.GONE);
		
		courseTaLayout = (LinearLayout) rootView.findViewById(R.id.course_ta_layout);
		courseTaLayout.setVisibility(View.GONE);
		
		groupAdminLayout = (LinearLayout) rootView.findViewById(R.id.group_admin_layout);
		groupAdminLayout.setVisibility(View.VISIBLE);
		
		admins = (ListView) rootView.findViewById(R.id.group_admin_list);
		
		adminAdapter = new CourseListAdapter(getActivity(), "user", GroupInformationFragment.admins);
		admins.setAdapter(adminAdapter);
		
		members = (ListView) rootView.findViewById(R.id.course_members_list);
		
		memberAdapter = new CourseListAdapter(getActivity(), "user", GroupInformationFragment.members);
		members.setAdapter(memberAdapter);
		
		return rootView;
	}
	
	
}
