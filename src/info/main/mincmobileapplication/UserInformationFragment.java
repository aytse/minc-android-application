package info.main.mincmobileapplication;

import info.server.mincmobileapplication.ConnectionFunctions;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UserInformationFragment extends Fragment{
	LinearLayout genderLayout, affiliationLayout, positionLayout,
	aboutMeLayout, mobilePhoneLayout, landPhoneLayout, addressLayout,
	stateLayout, cityLayout, countryLayout, websiteLayout,
	collegeLayout, gradYearLayout, expertLayout, intermediateLayout,
	beginnerLayout, othersLayout;
	
	TextView gender, affiliation, position, aboutMe,
	mobilePhone, landPhone, address, state, city, country,
	website, college, gradYear, expert, intermediate,
	beginner, others;
	
	String currentGender, currentAffiliation, currentPosition, currentAboutMe,
	currentMobilePhone, currentLandPhone, currentAddress, currentState, currentCity, currentCountry,
	currentWebsite, currentCollege, currentGradYear, currentExpert, currentIntermediate,
	currentBeginner, currentOthers;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.user_information_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		if (ConnectionFunctions.MYPROFILE) {
			currentGender = userSettings.getString(ConnectionFunctions.GENDER, null);
			currentAffiliation = userSettings.getString(ConnectionFunctions.AFFILIATION, null);
			currentPosition = userSettings.getString(ConnectionFunctions.POSITION, null);
			currentAboutMe = userSettings.getString(ConnectionFunctions.ABOUTME, null);
			currentMobilePhone = userSettings.getString(ConnectionFunctions.MOBILE, null);
			currentLandPhone = userSettings.getString(ConnectionFunctions.LAND, null);
			currentAddress = userSettings.getString(ConnectionFunctions.ADDRESS, null);
			currentState = userSettings.getString(ConnectionFunctions.STATE, null);
			currentCity = userSettings.getString(ConnectionFunctions.CITY, null);
			currentCountry = userSettings.getString(ConnectionFunctions.COUNTRY, null);
			currentWebsite = userSettings.getString(ConnectionFunctions.WEBSITE, null);
			currentCollege = userSettings.getString(ConnectionFunctions.COLLEGE, null);
			currentGradYear = userSettings.getString(ConnectionFunctions.GRADUATE, null);
			currentExpert = userSettings.getString(ConnectionFunctions.EXPERT, null);
			currentIntermediate = userSettings.getString(ConnectionFunctions.INTERMEDIATE, null);
			currentBeginner = userSettings.getString(ConnectionFunctions.BEGINNER, null);
			currentOthers = userSettings.getString(ConnectionFunctions.OTHERS, null);
		}
		
		else {
			currentGender = userSettings.getString(ConnectionFunctions.SELECTEDGENDER, null);
			currentAffiliation = userSettings.getString(ConnectionFunctions.SELECTEDAFFILIATION, null);
			currentPosition = userSettings.getString(ConnectionFunctions.SELECTEDPOSITION, null);
			currentAboutMe = userSettings.getString(ConnectionFunctions.SELECTEDABOUTME, null);
			currentMobilePhone = userSettings.getString(ConnectionFunctions.SELECTEDMOBILE, null);
			currentLandPhone = userSettings.getString(ConnectionFunctions.SELECTEDLAND, null);
			currentAddress = userSettings.getString(ConnectionFunctions.SELECTEDADDRESS, null);
			currentState = userSettings.getString(ConnectionFunctions.SELECTEDSTATE, null);
			currentCity = userSettings.getString(ConnectionFunctions.SELECTEDCITY, null);
			currentCountry = userSettings.getString(ConnectionFunctions.SELECTEDCOUNTRY, null);
			currentWebsite = userSettings.getString(ConnectionFunctions.SELECTEDWEBSITE, null);
			currentCollege = userSettings.getString(ConnectionFunctions.SELECTEDCOLLEGE, null);
			currentGradYear = userSettings.getString(ConnectionFunctions.SELECTEDGRADUATE, null);
			currentExpert = userSettings.getString(ConnectionFunctions.SELECTEDEXPERT, null);
			currentIntermediate = userSettings.getString(ConnectionFunctions.SELECTEDINTERMEDIATE, null);
			currentBeginner = userSettings.getString(ConnectionFunctions.SELECTEDBEGINNER, null);
			currentOthers = userSettings.getString(ConnectionFunctions.SELECTEDOTHERS, null);
		}
		genderLayout = (LinearLayout) rootView.findViewById(R.id.gender_layout);
		affiliationLayout = (LinearLayout) rootView.findViewById(R.id.affiliation_layout);
		positionLayout = (LinearLayout) rootView.findViewById(R.id.position_layout);
		aboutMeLayout = (LinearLayout) rootView.findViewById(R.id.aboutMe_layout);
		mobilePhoneLayout = (LinearLayout) rootView.findViewById(R.id.mobilePhone_layout);
		landPhoneLayout = (LinearLayout) rootView.findViewById(R.id.landPhone_layout);
		addressLayout = (LinearLayout) rootView.findViewById(R.id.address_layout);
		stateLayout = (LinearLayout) rootView.findViewById(R.id.state_layout);
		cityLayout = (LinearLayout) rootView.findViewById(R.id.city_layout);
		countryLayout = (LinearLayout) rootView.findViewById(R.id.country_layout);
		websiteLayout = (LinearLayout) rootView.findViewById(R.id.website_layout);
		collegeLayout = (LinearLayout) rootView.findViewById(R.id.college_layout);
		gradYearLayout = (LinearLayout) rootView.findViewById(R.id.gradYear_layout);
		expertLayout = (LinearLayout) rootView.findViewById(R.id.expert_layout);
		intermediateLayout = (LinearLayout) rootView.findViewById(R.id.intermediate_layout);
		beginnerLayout = (LinearLayout) rootView.findViewById(R.id.beginner_layout);
		othersLayout = (LinearLayout) rootView.findViewById(R.id.others_layout);
		
		gender = (TextView) rootView.findViewById(R.id.gender);
		affiliation = (TextView) rootView.findViewById(R.id.affiliation);
		position = (TextView) rootView.findViewById(R.id.position);
		aboutMe = (TextView) rootView.findViewById(R.id.aboutMe);
		mobilePhone = (TextView) rootView.findViewById(R.id.mobilePhone);
		landPhone = (TextView) rootView.findViewById(R.id.landPhone);
		address = (TextView) rootView.findViewById(R.id.address);
		state = (TextView) rootView.findViewById(R.id.state);
		city = (TextView) rootView.findViewById(R.id.city);
		country = (TextView) rootView.findViewById(R.id.country);
		website = (TextView) rootView.findViewById(R.id.website);
		college = (TextView) rootView.findViewById(R.id.college);
		gradYear = (TextView) rootView.findViewById(R.id.grad_year);
		expert = (TextView) rootView.findViewById(R.id.expert);
		intermediate = (TextView) rootView.findViewById(R.id.intermediate);
		beginner = (TextView) rootView.findViewById(R.id.beginner);
		others = (TextView) rootView.findViewById(R.id.others);
		
		if(currentGender.equals("null")||currentGender.equals(""))
			genderLayout.setVisibility(View.GONE);
		else
			gender.setText(currentGender);
		
		if(currentAffiliation.equals("null")||currentAffiliation.equals(""))
			affiliationLayout.setVisibility(View.GONE);
		else
			affiliation.setText(currentAffiliation);
		
		if(currentPosition.equals("null")||currentPosition.equals(""))
			positionLayout.setVisibility(View.GONE);
		else
			position.setText(currentPosition);
		
		if(currentAboutMe.equals("null")||currentAboutMe.equals(""))
			aboutMeLayout.setVisibility(View.GONE);
		else
			aboutMe.setText(currentAboutMe);
		
		if(currentMobilePhone.equals("null")||currentMobilePhone.equals(""))
			mobilePhoneLayout.setVisibility(View.GONE);
		else
			mobilePhone.setText(currentMobilePhone);
		
		if(currentLandPhone.equals("null")||currentLandPhone.equals(""))
			landPhoneLayout.setVisibility(View.GONE);
		else
			landPhone.setText(currentLandPhone);
		
		if(currentAddress.equals("null")||currentAddress.equals(""))
			addressLayout.setVisibility(View.GONE);
		else
			address.setText(currentAddress);
		
		if(currentState.equals("null")||currentState.equals(""))
			stateLayout.setVisibility(View.GONE);
		else
			state.setText(currentState);
		
		if(currentCity.equals("null")||currentCity.equals(""))
			cityLayout.setVisibility(View.GONE);
		else
			city.setText(currentCity);
		
		if(currentCountry.equals("null")||currentCountry.equals(""))
			countryLayout.setVisibility(View.GONE);
		else
			country.setText(currentCountry);
		
		if(currentWebsite.equals("null")||currentWebsite.equals(""))
			websiteLayout.setVisibility(View.GONE);
		else
			website.setText(currentWebsite);
		
		if(currentCollege.equals("null")||currentCollege.equals(""))
			collegeLayout.setVisibility(View.GONE);
		else
			college.setText(currentCollege);
		
		if(currentGradYear.equals("null")||currentGradYear.equals(""))
			gradYearLayout.setVisibility(View.GONE);
		else
			gradYear.setText(currentGradYear);
		
		if(currentExpert.equals("null")||currentExpert.equals(""))
			expertLayout.setVisibility(View.GONE);
		else
			expert.setText(currentExpert);
		
		if(currentIntermediate.equals("null")||currentIntermediate.equals(""))
			intermediateLayout.setVisibility(View.GONE);
		else
			intermediate.setText(currentIntermediate);
		
		if(currentBeginner.equals("null")||currentBeginner.equals(""))
			beginnerLayout.setVisibility(View.GONE);
		else
			beginner.setText(currentBeginner);
		
		if(currentOthers.equals("null")||currentOthers.equals(""))
			othersLayout.setVisibility(View.GONE);
		else
			others.setText(currentOthers);
		
		return rootView;
	}
	
	
}
