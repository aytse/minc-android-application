package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.LibraryAdapter;
import info.internal.mincmobileapplication.DownloadImagesTask;
import info.model.mincmobileapplication.ElementItem;
import info.server.mincmobileapplication.ConnectionFunctions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CourseInformationFragment extends Fragment{
	TextView yearTerm, time, location, hashTag,
	description;

	RelativeLayout course_members;
	
	ImageView member_pic1, member_pic2, member_pic3, member_pic4;
	TextView member_name1, member_name2, member_name3, member_name4;
	
	ListView course_library;
	
	JSONArray jArr, member_jArr, instructor_jArr, ta_jArr;
	
	public static ArrayList<ElementItem> instructors;
	public static ArrayList<ElementItem> tas;
	public static ArrayList<ElementItem> members;
	
	public static ArrayList<ElementItem> folders;
	
	LibraryAdapter adapter;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.course_group_information_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		yearTerm = (TextView) rootView.findViewById(R.id.year_term_info);
		yearTerm.setText(userSettings.getString(ConnectionFunctions.COURSETERM, null)+" "+
				userSettings.getString(ConnectionFunctions.COURSEYEAR, null));
		time = (TextView) rootView.findViewById(R.id.course_time_info);
		time.setText(userSettings.getString(ConnectionFunctions.COURSESTART, null));
		location = (TextView) rootView.findViewById(R.id.location_info);
		location.setText(userSettings.getString(ConnectionFunctions.COURSELOCATION, null));
		hashTag = (TextView) rootView.findViewById(R.id.hashtag_info);
		hashTag.setText(userSettings.getString(ConnectionFunctions.COURSEHASHTAG, null));
		description = (TextView) rootView.findViewById(R.id.description_info);
		description.setText(userSettings.getString(ConnectionFunctions.COURSEDESCRIPTION, null));
		
		instructors = new ArrayList<ElementItem>();
		tas = new ArrayList<ElementItem>();
		members = new ArrayList<ElementItem>();
		
		folders = new ArrayList<ElementItem>();
		
		String getMembersCall = ConnectionFunctions.FUNCTIONBASEURL+"getCourseMembers.php?courseid="+userSettings.getString(ConnectionFunctions.COURSEID, null);
		String getCourseLibraryCall = ConnectionFunctions.FUNCTIONBASEURL+"getCourseLibraryList_courseid.php?courseid="+userSettings.getString(ConnectionFunctions.COURSEID, null);
		new CourseInformationTask("load_members").execute(getMembersCall);
		new CourseInformationTask("load_course_library").execute(getCourseLibraryCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
		
		member_pic1 = (ImageView) rootView.findViewById(R.id.member_pic_1);
		member_name1 = (TextView) rootView.findViewById(R.id.member_name_1);
		member_pic2 = (ImageView) rootView.findViewById(R.id.member_pic_2);
		member_name2 = (TextView) rootView.findViewById(R.id.member_name_2);
		member_pic3 = (ImageView) rootView.findViewById(R.id.member_pic_3);
		member_name3 = (TextView) rootView.findViewById(R.id.member_name_3);
		member_pic4 = (ImageView) rootView.findViewById(R.id.member_pic_4);
		member_name4 = (TextView) rootView.findViewById(R.id.member_name_4);
		
		course_members = (RelativeLayout) rootView.findViewById(R.id.course_members);
		course_members.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((MainActivity) getActivity()).changeFragment(new CourseMemberFragment(), userSettings.getString(ConnectionFunctions.COURSENAME, null));
			}
			
		});
		
		course_library = (ListView) rootView.findViewById(R.id.course_library_list);
		course_library.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				userSettingsEditor.putString(ConnectionFunctions.FOLDERID, folders.get(position).getId());
				userSettingsEditor.putString(ConnectionFunctions.FOLDERNAME, folders.get(position).getName());
				userSettingsEditor.commit();
				((MainActivity) getActivity()).changeFragment(new CourseFolderFragment(), userSettings.getString(ConnectionFunctions.FOLDERNAME, null));
			}
			
		});
		
		adapter = new LibraryAdapter(getActivity(), folders);
		course_library.setAdapter(adapter);
		
		return rootView;
	}
	
	private void postCheck(String task, String result) {
		try {
			if(task.equals("load_members")) {
				jArr = new JSONArray(result);
				
				for(int i=0; i<jArr.length(); i++) {
					if(jArr.getJSONObject(i).getString("type").equals("Member")) {
						member_jArr = new JSONArray(jArr.getJSONObject(i).getString("data"));
						for(int j=0; j<member_jArr.length();j++) {
							ElementItem e = new ElementItem();
							e.setId(member_jArr.getJSONObject(j).getString("userid"));
							e.setName(member_jArr.getJSONObject(j).getString("name"));
							//member_work.add(member_jArr.getJSONObject(j).getString(""));
							e.setAvatar(member_jArr.getJSONObject(j).getString("avatar"));
							members.add(e);
						}
						member_pic3.setTag(members.get(0).getAvatar());
						new DownloadImagesTask(getActivity()).execute(member_pic3);
						member_pic4.setTag(members.get(1).getAvatar());
						new DownloadImagesTask(getActivity()).execute(member_pic4);
						
						member_name3.setText(members.get(0).getName());
						member_name4.setText(members.get(1).getName());
					}
					else if(jArr.getJSONObject(i).getString("type").equals("Instructor")) {
						instructor_jArr = new JSONArray(jArr.getJSONObject(i).getString("data"));
						for(int j=0; j<instructor_jArr.length();j++) {
							ElementItem e = new ElementItem();
							e.setId(instructor_jArr.getJSONObject(j).getString("userid"));
							e.setName(instructor_jArr.getJSONObject(j).getString("name"));
							//instructor_work.add(instructor_jArr.getJSONObject(j).getString(""));
							e.setAvatar(instructor_jArr.getJSONObject(j).getString("avatar"));
							instructors.add(e);
						}
						member_pic1.setTag(instructors.get(0).getAvatar());
						new DownloadImagesTask(getActivity()).execute(member_pic1);
						
						member_name1.setText(instructors.get(0).getName());
						
					}
					else if(jArr.getJSONObject(i).getString("type").equals("TA")) {
						ta_jArr = new JSONArray(jArr.getJSONObject(i).getString("data"));
						for(int k=0; k<ta_jArr.length();k++) {
							ElementItem e = new ElementItem();
							e.setId(ta_jArr.getJSONObject(k).getString("userid"));
							e.setName(ta_jArr.getJSONObject(k).getString("name"));
							//ta_work.add(ta_jArr.getJSONObject(k).getString(""));
							e.setAvatar(ta_jArr.getJSONObject(k).getString("avatar"));
							tas.add(e);
						}
						member_pic2.setTag(tas.get(0).getAvatar());
						new DownloadImagesTask(getActivity()).execute(member_pic2);
						
						member_name2.setText(tas.get(0).getName());
						
					}
				}
			}
			
			else if(task.equals("load_course_library")) {
				jArr = new JSONArray(result);
				
				for(int i = 0; i<jArr.length(); i++) {
					ElementItem e = new ElementItem();
					e.setId(jArr.getJSONObject(i).getString("id"));
					e.setName(jArr.getJSONObject(i).getString("name"));
					folders.add(e);
				}
				adapter.notifyDataSetChanged();
			}
			
		} catch (JSONException e) {
			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	private class CourseInformationTask extends AsyncTask<String, Void, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public CourseInformationTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}	
	}
}
