package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.FolderListAdapter;
import info.model.mincmobileapplication.FolderItem;
import info.server.mincmobileapplication.ConnectionFunctions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class CourseFolderFragment extends Fragment{
	TextView folderName;
	ListView documentList;
	
	FolderListAdapter adapter;

	JSONArray jArr;
	
	ArrayList<FolderItem> files;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.folder_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		folderName = (TextView) rootView.findViewById(R.id.course_folder_title);
		folderName.setText(userSettings.getString(ConnectionFunctions.FOLDERNAME, null));
	
		files = new ArrayList<FolderItem>();
		
		String getDocumentsCall = ConnectionFunctions.FUNCTIONBASEURL+"getURLinCourseLibrary.php?collectionid="+userSettings.getString(ConnectionFunctions.FOLDERID, null);
		new CourseFolderTask("load_documents").execute(getDocumentsCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
		
		documentList = (ListView) rootView.findViewById(R.id.course_document_list);

		adapter = new FolderListAdapter(getActivity(), files);
		documentList.setAdapter(adapter);
		return rootView;
	}
	
	private void postCheck(String task, String result) {
		try {
			if(task.equals("load_documents")) {
				jArr = new JSONArray(result);
				
				for(int i=0; i<jArr.length(); i++) {
					FolderItem f = new FolderItem();
					f.setId(jArr.getJSONObject(i).getString("id"));
					f.setName(jArr.getJSONObject(i).getString("caption"));
					f.setIcon(jArr.getJSONObject(i).getString("thumbnail"));
					f.setUrl(jArr.getJSONObject(i).getString("original"));
					f.setDescription(jArr.getJSONObject(i).getString("description"));
					files.add(f);
				}
				adapter.notifyDataSetChanged();
			}		
		} catch (JSONException e) {			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	private class CourseFolderTask extends AsyncTask<String, View, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public CourseFolderTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}	
	}
}
