package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.CourseListAdapter;
import info.model.mincmobileapplication.ElementItem;
import info.server.mincmobileapplication.ConnectionFunctions;
import info.server.mincmobileapplication.ConnectionTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

public class SearchFragment extends Fragment{

	EditText searchInput;
	Spinner searchSpinner;
	Button searchButton;
	public static ListView searchList;
	
	String searchCall;
	String selectedType;
	
	ArrayAdapter<String> spinnerAdapter;

	JSONArray jArr;
	ArrayList<ElementItem>searches;
	CourseListAdapter adapter;
	
	String[] types;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	public SearchFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.search_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		searchInput = (EditText) rootView.findViewById(R.id.searchInput);
		
		types = new String[] {
				"Select type", "User", "Question"
		};
		spinnerAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, types);
		
		searchSpinner = (Spinner) rootView.findViewById(R.id.searchSpinner);
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
	    searchSpinner.setAdapter(spinnerAdapter);
		
		searchButton = (Button) rootView.findViewById(R.id.searchButton);
		searchButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				selectedType =String.valueOf(searchSpinner.getSelectedItem());
				String input = searchInput.getText().toString();
				
				if(selectedType.equals("User")) {
					searches.clear();
					
					searchCall = ConnectionFunctions.FUNCTIONBASEURL+"getAllUsers_keyword.php?keyword="+input;
					new SearchTask("search_user").execute(searchCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Searching users...", true, false);
				}
				else if(selectedType.equals("Question")) {
					searches.clear();
					
					searchCall = ConnectionFunctions.FUNCTIONBASEURL+"getAllQuestions_keyword.php?keyword="+input;
					new SearchTask("search_question").execute(searchCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Searching questions...", true, false);
				}
			}
			
		});
		
		searches = new ArrayList<ElementItem>();
		
		searchList = (ListView) rootView.findViewById(R.id.searchList);
		searchList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				String currentElement = searches.get(position).getId();
				if(selectedType.equals("User")) {
					userSettingsEditor.putString(ConnectionFunctions.SELECTEDID, currentElement);
					userSettingsEditor.commit();
					adapter.setType("user");
					String openUserCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserOtherInfo_userid.php?userid="+currentElement;
					new ConnectionTask(getActivity(),"open_user").execute(openUserCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
				}
				else if(selectedType.equals("Question")) {
					adapter.setType("question");
					String openQuestionCall = ConnectionFunctions.FUNCTIONBASEURL+"getQuestion_questionid.php?questionid="+ currentElement;
					new ConnectionTask(getActivity(),"open_question").execute(openQuestionCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
				}
				
			}
			
		});	
		
		adapter = new CourseListAdapter(getActivity(), "user", searches);
		searchList.setAdapter(adapter);
		
		return rootView;
	}
	
	private void postCheck(String task, String result) {
		try {
			if(task.equals("search_user")) {
				jArr = new JSONArray(result);
				adapter.setType("user");
				for (int i=0; i<jArr.length(); i++) {
					ElementItem foundSearch = new ElementItem();
					foundSearch.setId(jArr.getJSONObject(i).getString("id"));
					foundSearch.setName(jArr.getJSONObject(i).getString("name"));
					foundSearch.setAvatar(jArr.getJSONObject(i).getString("avatar"));
					searches.add(foundSearch);
				}
				adapter.notifyDataSetChanged();
			}
			else if(task.equals("search_question")) {
				jArr = new JSONArray(result);
				adapter.setType("question");
				for (int i=0; i<jArr.length(); i++) {
					ElementItem foundSearch = new ElementItem();
					foundSearch.setId(jArr.getJSONObject(i).getString("id"));
					foundSearch.setName(jArr.getJSONObject(i).getString("title"));
					foundSearch.setAvatar(jArr.getJSONObject(i).getString("avatar"));
					searches.add(foundSearch);
				}
				adapter.notifyDataSetChanged();
			}
		} catch (JSONException e) {
			
		} finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	private class SearchTask extends AsyncTask<String, Void, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public SearchTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}	
	}
}
