package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.CourseListAdapter;
import info.model.mincmobileapplication.ElementItem;
import info.server.mincmobileapplication.ConnectionFunctions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class ConnectionFragment extends Fragment{

	EditText searchInput;
	Button searchButton;
	ListView connectionsList;
	
	JSONArray jArr;
	ArrayList<ElementItem> connections;
	CourseListAdapter connectionAdapter;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	public ConnectionFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.connection_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		searchInput = (EditText) rootView.findViewById(R.id.connection_search_input);
		
		searchButton = (Button) rootView.findViewById(R.id.connection_search_button);
		searchButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String text = searchInput.getText().toString().toLowerCase(Locale.getDefault());
				connectionAdapter.filter(text);
			}
			
		});

		connectionsList = (ListView) rootView.findViewById(R.id.connection_list);

		String getConnectionsCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserConnections.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null);
		new ConnectionListViewTask("load_connections").execute(getConnectionsCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading connections...", true, false);
		
		return rootView;
	}
	
	private void postCheck(String task, String result) {
		try {
			if (task.equals("load_connections")) {
				JSONArray  jArr = new JSONArray(result);
				
				connections = new ArrayList<ElementItem>();
				
				for (int i = 0; i < jArr.length(); i++) {
					ElementItem connection = new ElementItem();
					connection.setId(jArr.getJSONObject(i).getString("userid"));
					connection.setName(jArr.getJSONObject(i).getString("name"));
					connection.setAvatar(jArr.getJSONObject(i).getString("avatar"));
					connection.setType("user");
					connections.add(connection);
				}
				if (connectionAdapter == null) {
					connectionAdapter = new CourseListAdapter(getActivity(), "user", connections);
					connectionsList.setAdapter(connectionAdapter);
				} else
					connectionAdapter.notifyDataSetChanged();

				ConnectionFunctions.pdLoadFeed.dismiss();
			}
		} catch (JSONException e) {
			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	private class ConnectionListViewTask extends AsyncTask<String, Void, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public ConnectionListViewTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}
		
		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}
	}
	
}
