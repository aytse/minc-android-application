package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.CourseListAdapter;
import info.model.mincmobileapplication.ElementItem;
import info.server.mincmobileapplication.ConnectionFunctions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class GroupListFragment extends Fragment{
	TextView groupListName;
	ListView groupList;
	
	JSONArray jArr;
	ArrayList<ElementItem> groups;
	CourseListAdapter groupAdapter;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.folder_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		groupListName = (TextView) rootView.findViewById(R.id.course_folder_title);
		groupListName.setText("All Groups");
		
		groups = new ArrayList<ElementItem>();
		
		String getGroupsCall = ConnectionFunctions.FUNCTIONBASEURL+"getAllGroups.php";
		new GroupListTask("load_all_groups").execute(getGroupsCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
		
		groupList = (ListView) rootView.findViewById(R.id.course_document_list);
		
		return rootView;
	}

	private void postCheck(String task, String result) {
		try {
			if(task.equals("load_all_groups")) {
				jArr = new JSONArray(result);
				
				for(int i=0; i<jArr.length(); i++) {
					ElementItem g = new ElementItem();
					g.setId(jArr.getJSONObject(i).getString("id"));
					g.setName(jArr.getJSONObject(i).getString("name"));
					g.setAvatar(jArr.getJSONObject(i).getString("avatar"));
					g.setType("group");
					groups.add(g);
				}
				if(groupAdapter == null) {
					groupAdapter = new CourseListAdapter(getActivity(), "group", groups);
					groupList.setAdapter(groupAdapter);
				} else
					groupAdapter.notifyDataSetChanged();
				
				ConnectionFunctions.pdLoadFeed.dismiss();
			}
		} catch (JSONException e) {
			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	private class GroupListTask extends AsyncTask<String, View, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public GroupListTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
				
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}	
	}
}