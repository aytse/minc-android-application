package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.CourseListAdapter;
import info.server.mincmobileapplication.ConnectionFunctions;
import info.server.mincmobileapplication.ConnectionTask;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class CourseMemberFragment extends Fragment{
	ListView instructors, tas, members;
	
	CourseListAdapter memberAdapter;
	CourseListAdapter instructorAdapter;
	CourseListAdapter taAdapter;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.course_member_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		instructors = (ListView) rootView.findViewById(R.id.course_instructor_list);
		
		instructorAdapter = new CourseListAdapter(getActivity(), "user", CourseInformationFragment.instructors);
		instructors.setAdapter(instructorAdapter);
		
		tas = (ListView) rootView.findViewById(R.id.course_ta_list);
		
		taAdapter = new CourseListAdapter(getActivity(), "user", CourseInformationFragment.tas);
		tas.setAdapter(taAdapter);
		
		members = (ListView) rootView.findViewById(R.id.course_members_list);
		
		memberAdapter = new CourseListAdapter(getActivity(), "user", CourseInformationFragment.members);
		members.setAdapter(memberAdapter);
		
		return rootView;}
}
