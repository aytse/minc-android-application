package info.main.mincmobileapplication;

import info.internal.mincmobileapplication.DownloadImagesTask;
import info.internal.mincmobileapplication.StringFormatter;
import info.model.mincmobileapplication.ReplyItem;
import info.server.mincmobileapplication.ConnectionFunctions;
import info.server.mincmobileapplication.ConnectionTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class QuestionFragment extends Fragment{
	String currentReplierId;
	
	LinearLayout userLink;
	ImageView askerProfilePic;
	TextView questionTitle, asker, question, time,
	myRating, avgRating, numberRate;
	EditText postReplyInput;
	Button postReplyButton, questionRateButton;
	
	JSONArray jArr;
	BrowseRepliesAdapter adapter;
	ArrayList<ReplyItem> replies;
	
	ListView repliesList;
	
	String currentReplyId;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	public QuestionFragment() {}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.question_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		userLink = (LinearLayout) rootView.findViewById(R.id.user_link);
		userLink.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
				currentReplierId = userSettings.getString(ConnectionFunctions.ASKERID, null);
				userSettingsEditor.putString(ConnectionFunctions.SELECTEDID, currentReplierId);
				ConnectionFunctions.MYPROFILE=false;
				userSettingsEditor.commit();
				String openUserCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserOtherInfo_userid.php?userid="+userSettings.getString(ConnectionFunctions.SELECTEDID, null);
				new ConnectionTask(getActivity(), "open_user").execute(openUserCall);
			}
			
		});
		
		questionTitle = (TextView) rootView.findViewById(R.id.question_title);
		questionTitle.setText(userSettings.getString(ConnectionFunctions.QUESTIONNAME, null));
		
		askerProfilePic = (ImageView) rootView.findViewById(R.id.asker_profile_pic);
		askerProfilePic.setTag(userSettings.getString(ConnectionFunctions.ASKERAVATAR, null));
		new DownloadImagesTask(getActivity()).execute(askerProfilePic);
		
		asker = (TextView) rootView.findViewById(R.id.asker_name);
		asker.setText(userSettings.getString(ConnectionFunctions.ASKERNAME, null));
		question = (TextView) rootView.findViewById(R.id.question);
		question.setText(userSettings.getString(ConnectionFunctions.QUESTION, null));
		time = (TextView) rootView.findViewById(R.id.time_question);
		time.setText(userSettings.getString(ConnectionFunctions.QUESTIONTIME, null));
		
		if(userSettings.getString(ConnectionFunctions.QUESTIONAVGRATING, null).equals("null"))
			userSettingsEditor.putString(ConnectionFunctions.QUESTIONAVGRATING, "");
		if(userSettings.getString(ConnectionFunctions.QUESTIONMYRATING, null).equals("null"))
			userSettingsEditor.putString(ConnectionFunctions.QUESTIONMYRATING, "");
		if(userSettings.getString(ConnectionFunctions.QUESTIONNUMBERRATE, null).equals("null"))
			userSettingsEditor.putString(ConnectionFunctions.QUESTIONNUMBERRATE, "0");
		userSettingsEditor.commit();
		
		myRating = (TextView) rootView.findViewById(R.id.my_question_rating_info);
		myRating.setText(userSettings.getString(ConnectionFunctions.QUESTIONMYRATING, null)+"/5");
		avgRating = (TextView) rootView.findViewById(R.id.question_rating_info);
		avgRating.setText(userSettings.getString(ConnectionFunctions.QUESTIONAVGRATING, null)+"/5");
		numberRate = (TextView) rootView.findViewById(R.id.question_number_rate_info);
		numberRate.setText("("+userSettings.getString(ConnectionFunctions.QUESTIONNUMBERRATE, null)+" raters)");
		
		questionRateButton = (Button) rootView.findViewById(R.id.questionRateButton);
		questionRateButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle("Rating reply")
				.setItems(ConnectionFunctions.ratings, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int rateposition) {
						String rateResourceCall = ConnectionFunctions.FUNCTIONBASEURL+"setResourceRating_userid_resrcid_rating.php?userid="+
					userSettings.getString(ConnectionFunctions.USERID, null)+"&resrcid="+userSettings.getString(ConnectionFunctions.QUESTIONID, null)+"&rating="+ConnectionFunctions.ratings[rateposition];
						new QuestionTask("rate_question").execute(rateResourceCall);
						ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
					}
					
				});
				AlertDialog dialog = builder.create();
				dialog.show();
				}
			});
		
		postReplyInput = (EditText) rootView.findViewById(R.id.postReplyInput);
		
		postReplyButton = (Button) rootView.findViewById(R.id.postReplyButton);
		postReplyButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// POSTS REPLY AND UPDATES LISTS OF REPLIES
				if(!postReplyInput.getText().toString().equals("")) {
					String reply = postReplyInput.getText().toString().replaceAll(" ", "%20");
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Posting reply...", true, false);
					String setReplyCall = ConnectionFunctions.FUNCTIONBASEURL+"setQuestionReplies_questionid_userid_targetid_content.php?questionid="+userSettings.getString(ConnectionFunctions.QUESTIONID, null)+
						"&userid="+userSettings.getString(ConnectionFunctions.USERID, null)+"&targetid="+userSettings.getString(ConnectionFunctions.ASKERID, null)+"&content"+reply;
					new ConnectionTask(getActivity(), "post_reply").execute(setReplyCall);
				}
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(postReplyInput.getWindowToken(), 0);
			}	
		});
		
		replies = new ArrayList<ReplyItem>();
		
		String getRepliesCall = ConnectionFunctions.FUNCTIONBASEURL+"getQuestionReplies_questionid_userid.php?questionid="+userSettings.getString(ConnectionFunctions.QUESTIONID, null)+
				"&userid="+userSettings.getString(ConnectionFunctions.USERID, null);		
		new QuestionTask("load_replies").execute(getRepliesCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading replies...", true, false);
		
		repliesList = (ListView) rootView.findViewById(R.id.replies_list);
		
		adapter = new BrowseRepliesAdapter(replies);
		repliesList.setAdapter(adapter);
		
		return rootView;
	}
	
	private void postCheck(String task, String result) {
		try {
			if (task.equals("load_replies")) {
				jArr = new JSONArray(result);
				StringFormatter s = new StringFormatter();

				for (int i=0; i<jArr.length(); i++) {
					ReplyItem currentReply = new ReplyItem();
					currentReply.setReplyId(jArr.getJSONObject(i).getString("id"));
					currentReply.setReplierId(jArr.getJSONObject(i).getString("creator"));
					currentReply.setReplierName(jArr.getJSONObject(i).getString("name"));
					currentReply.setReplierIcon(jArr.getJSONObject(i).getString("avatar"));
					String replyFormat = s.format(jArr.getJSONObject(i).getString("comment"));
					currentReply.setReply(replyFormat);
					currentReply.setReplyTime(jArr.getJSONObject(i).getString("date"));
					currentReply.setReplyAverageRate(jArr.getJSONObject(i).getString("avgrating"));
					currentReply.setReplyNumberRate(jArr.getJSONObject(i).getString("totcount"));
					currentReply.setReplyMyRate(jArr.getJSONObject(i).getString("myrating"));
					replies.add(currentReply);
				}
				adapter.notifyDataSetChanged();
			}
			else if (task.equals("update_question")) {
				ConnectionFunctions.pdLoadFeed.dismiss();
				JSONObject jObj = new JSONArray(result).getJSONObject(0);
		
				userSettingsEditor.putString(ConnectionFunctions.QUESTIONMYRATING, jObj.getString("myrating"));
				userSettingsEditor.putString(ConnectionFunctions.QUESTIONAVGRATING, jObj.getString("avgrating"));
				userSettingsEditor.putString(ConnectionFunctions.QUESTIONNUMBERRATE, jObj.getString("totcount"));				
				userSettingsEditor.commit();
				
				if(userSettings.getString(ConnectionFunctions.QUESTIONAVGRATING, null).equals("null"))
					userSettingsEditor.putString(ConnectionFunctions.QUESTIONAVGRATING, "");
				if(userSettings.getString(ConnectionFunctions.QUESTIONMYRATING, null).equals("null"))
					userSettingsEditor.putString(ConnectionFunctions.QUESTIONMYRATING, "");
				if(userSettings.getString(ConnectionFunctions.QUESTIONNUMBERRATE, null).equals("null"))
					userSettingsEditor.putString(ConnectionFunctions.QUESTIONNUMBERRATE, "0");
				userSettingsEditor.commit();
				
				myRating.setText(userSettings.getString(ConnectionFunctions.QUESTIONMYRATING, null)+"/5");
				avgRating.setText(userSettings.getString(ConnectionFunctions.QUESTIONAVGRATING, null)+"/5");
				numberRate.setText("("+userSettings.getString(ConnectionFunctions.QUESTIONNUMBERRATE, null)+" raters)");
			}
			else if(task.equals("update_reply")) {
				for (int i=0; i<jArr.length(); i++) {
					replies.get(i).setReplyAverageRate(null);
					replies.get(i).setReplyMyRate(null);
					replies.get(i).setReplyNumberRate(null);
				}
				jArr = new JSONArray(result);
				
				for (int i=0; i<jArr.length(); i++) {
					replies.get(i).setReplyAverageRate(jArr.getJSONObject(i).getString("avgrating"));
					replies.get(i).setReplyNumberRate(jArr.getJSONObject(i).getString("totcount"));
					replies.get(i).setReplyMyRate(jArr.getJSONObject(i).getString("myrating"));
				}
				adapter.notifyDataSetChanged();
			}
			else if(task.equals("rate_question")) {
				ConnectionFunctions.pdLoadFeed.dismiss();
				new QuestionTask("update_question").execute(ConnectionFunctions.FUNCTIONBASEURL+"getQuestion_questionid_userid.php?questionid="+ userSettings.getString(ConnectionFunctions.QUESTIONID, null)+
						"&userid="+userSettings.getString(ConnectionFunctions.USERID, null));
			}
			else if(task.equals("rate_reply")) {
				ConnectionFunctions.pdLoadFeed.dismiss();
				new QuestionTask("update_reply").execute(ConnectionFunctions.FUNCTIONBASEURL+"getQuestionReplies_questionid_userid.php?questionid="+userSettings.getString(ConnectionFunctions.QUESTIONID, null)+
						"&userid="+userSettings.getString(ConnectionFunctions.USERID, null));
			}
			
		} catch (JSONException e) {
			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	class BrowseRepliesAdapter extends BaseAdapter {

		ArrayList<ReplyItem>replies;
		
		protected ListView mListView;
		
		BrowseRepliesAdapter() {
			replies = null;
		}
		
		public BrowseRepliesAdapter(ArrayList<ReplyItem>replies) {
			this.replies=replies;
		}
		
		@Override
		public int getCount() {
			return replies.size();
		}

		@Override
		public Object getItem(int position) {
			return replies.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			
			if (convertView == null) {
				holder = new ViewHolder();
				
				LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.question_list_element, parent, false);
				
				holder.userLink = (LinearLayout) convertView.findViewById(R.id.reply_layout);
				holder.replierPic = (ImageView) convertView.findViewById(R.id.replier_profile_pic);
				holder.name = (TextView) convertView.findViewById(R.id.replier_name);
				holder.reply = (TextView) convertView.findViewById(R.id.reply);
				holder.time = (TextView) convertView.findViewById(R.id.time_reply);
				holder.avgRate = (TextView) convertView.findViewById(R.id.reply_rating_info);
				holder.numRate = (TextView) convertView.findViewById(R.id.reply_number_rate_info);
				holder.myRate = (TextView) convertView.findViewById(R.id.my_reply_rating_info);
				holder.rateButton = (Button) convertView.findViewById(R.id.rateButton);
				
				convertView.setTag(holder);
			}
			else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			if(replies.get(position).getReplyAverageRate().equals("null"))
				replies.get(position).setReplyAverageRate("");
			if(replies.get(position).getReplyNumberRate().equals("null"))
				replies.get(position).setReplyNumberRate("0");
			if(replies.get(position).getReplyMyRate().equals("null"))
				replies.get(position).setReplyMyRate("");

			holder.name.setText(replies.get(position).getReplierName());
			holder.reply.setText(replies.get(position).getReply());
			holder.time.setText(replies.get(position).getReplyTime());
			holder.avgRate.setText(replies.get(position).getReplyAverageRate()+"/5");
			holder.numRate.setText("("+replies.get(position).getReplyNumberRate()+" raters)");
			holder.myRate.setText(replies.get(position).getReplyMyRate()+"/5");
			
			holder.replierPic.setTag(replies.get(position).getReplierIcon());
			new DownloadImagesTask(getActivity()).execute(holder.replierPic);
			
			holder.userLink.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					mListView = (ListView) v.getParent();
					int currentposition=mListView.getPositionForView(v); 
					currentReplierId=replies.get(currentposition).getReplierId();
					userSettingsEditor.putString(ConnectionFunctions.SELECTEDID, currentReplierId);
					ConnectionFunctions.MYPROFILE = false;
					userSettingsEditor.commit();
					String openUserCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserOtherInfo_userid.php?userid="+currentReplierId;
					new ConnectionTask(getActivity(), "open_user").execute(openUserCall);
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
				}
				
			});
			
			holder.rateButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					mListView = (ListView) v.getParent().getParent().getParent();
					int currentposition=mListView.getPositionForView((View) v.getParent()); 
					currentReplyId=replies.get(currentposition).getReplyId();
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("Rating reply")
					.setItems(ConnectionFunctions.ratings, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int rateposition) {
							String rateResourceCall = "http://hive.asu.edu/minc/PHP_Functions/setResourceRating_userid_resrcid_rating.php?userid="+
						userSettings.getString(ConnectionFunctions.USERID, null)+"&resrcid="+currentReplyId+"&rating="+ConnectionFunctions.ratings[rateposition];
							new QuestionTask("rate_reply").execute(rateResourceCall);
							ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
						}
						
					});
					AlertDialog dialog = builder.create();
					dialog.show();
				}
				
			});
			
			holder.rateButton.setTag(holder);
			
			return convertView;
		}
		
	}
	
	static class ViewHolder {
		LinearLayout userLink;
		ImageView replierPic;
		TextView name, reply, time, avgRate, numRate, myRate;
		Button rateButton;
	}
	
	private class QuestionTask extends AsyncTask<String, Void, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public QuestionTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}
	}
	
}
