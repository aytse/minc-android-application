package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.FolderListAdapter;
import info.model.mincmobileapplication.FolderItem;
import info.server.mincmobileapplication.ConnectionFunctions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class CollectionFragment extends Fragment{
	TextView collectionName;
	ListView collectionList;
	
	FolderListAdapter adapter;
	
	JSONArray jArr;
	
	ArrayList<FolderItem> collections;
	
	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;
	
	public CollectionFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.folder_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		collectionName = (TextView) rootView.findViewById(R.id.course_folder_title);
		collectionName.setText(userSettings.getString(ConnectionFunctions.NAME, null)+"'s collections");
		
		collections = new ArrayList<FolderItem>();
		
		String getCollectionsCall = ConnectionFunctions.FUNCTIONBASEURL+"getCollections_userid.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null);
		new CollectionTask("load_collections").execute(getCollectionsCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
		
		collectionList = (ListView) rootView.findViewById(R.id.course_document_list);
		
		return rootView;
	}
	
	private void postCheck(String task, String result) {
		try {
			if(task.equals("load_collections")) {
				jArr = new JSONArray(result);
				
				for(int i=0; i<jArr.length(); i++) {
					FolderItem c = new FolderItem();
					// NEED A GET COLLECTION FUNCTION
					c.setId(jArr.getJSONObject(i).getString("id"));
					c.setName(jArr.getJSONObject(i).getString("name"));
					c.setUrl(jArr.getJSONObject(i).getString("path"));
					c.setDescription("null");
					collections.add(c);
					
				}
				if(adapter == null) {
					adapter = new FolderListAdapter(getActivity(), collections);
					collectionList.setAdapter(adapter);
				} else
					adapter.notifyDataSetChanged();
				
				ConnectionFunctions.pdLoadFeed.dismiss();
			}		
		} catch (JSONException e) {
			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}
	
	private class CollectionTask extends AsyncTask<String, View, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public CollectionTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}	
	}
}
