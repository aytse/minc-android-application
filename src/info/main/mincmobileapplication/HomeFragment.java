package info.main.mincmobileapplication;

import info.adapter.mincmobileapplication.ActivityListAdapter;
import info.internal.mincmobileapplication.DownloadImagesTask;
import info.internal.mincmobileapplication.StringFormatter;
import info.model.mincmobileapplication.ActivityItem;
import info.server.mincmobileapplication.ConnectionFunctions;
import info.server.mincmobileapplication.ConnectionTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class HomeFragment extends Fragment{

	LinearLayout profileInformation;
	ImageView mainProfilePic;
	TextView myUsername, myWork;
	EditText postQuestionInput;
	Button postQuestionButton, setWorkButton;
	
	JSONArray jArr;
	ArrayList<ActivityItem> activities;
	ActivityListAdapter adapter;
	
	ListView activities_list;
	
	int activityCount=0;
	int newActivityCount=9;
	
	String inputWork;

	SharedPreferences userSettings;
	SharedPreferences.Editor userSettingsEditor;

	public HomeFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ConnectionFunctions.MYPROFILE=true;
		
		View rootView = inflater.inflate(R.layout.home_fragment, container, false);
		
		userSettings = this.getActivity().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		profileInformation = (LinearLayout) rootView.findViewById(R.id.profile_information);
		profileInformation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ConnectionFunctions.MYPROFILE=true;
				((MainActivity) getActivity()).changeFragment(new UserInformationFragment(), userSettings.getString(ConnectionFunctions.NAME, null));
			}
			
		});
		
		mainProfilePic = (ImageView) rootView.findViewById(R.id.profile_pic_large);
		mainProfilePic.setTag(userSettings.getString(ConnectionFunctions.AVATAR, null));
		new DownloadImagesTask(getActivity()).execute(mainProfilePic);
		
		myUsername = (TextView) rootView.findViewById(R.id.my_username);
		myUsername.setText(userSettings.getString(ConnectionFunctions.NAME, null));
		myWork = (TextView) rootView.findViewById(R.id.my_work);
		myWork.setText(userSettings.getString(ConnectionFunctions.WORK, null));
		
		setWorkButton = (Button) rootView.findViewById(R.id.set_work_button);
		setWorkButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setTitle("Currently working on");
				
				final EditText input = new EditText(getActivity());
				builder.setView(input);
				
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						inputWork = input.getText().toString();
						AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
						builder1.setTitle("Level")
						.setItems(ConnectionFunctions.levels, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int position) {
								String setCurrentWorkCall = ConnectionFunctions.FUNCTIONBASEURL+"setCurrentlyWork_userid_content_level.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null)+
										"&content="+inputWork+"&level="+ConnectionFunctions.levels[position];
								new ListViewTask("set_work").execute(setCurrentWorkCall);
								
								userSettingsEditor.putString(ConnectionFunctions.WORK, inputWork+"["+ConnectionFunctions.levels[position]+"]");
								myWork.setText(inputWork+"["+ConnectionFunctions.levels[position]+"]");
								
								ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading...", true, false);
							}					
						});
						AlertDialog alertDialog = builder1.create();
						alertDialog.show();
					}
					
				});
				
				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
					
				});
				
				builder.show();
			}
			
		});
		
		activities = new ArrayList<ActivityItem>();
		
		activities_list = (ListView) rootView.findViewById(R.id.activities_list);
		
		// Real code activities
		String getActivityCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserActivity_userid_from_to.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null)+"&from=0&to=9";
		
		new ListViewTask("load_activity").execute(getActivityCall);
		ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading activities...", true, false);
		
		activities_list.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScroll(AbsListView view, int firstVisible, int visibleCount, int totalCount) {
				
			}

			@Override
			public void onScrollStateChanged(AbsListView listView, int scrollState) {
				if (scrollState == SCROLL_STATE_IDLE) {
					if(listView.getLastVisiblePosition() >= listView.getCount() - 1) {
						activityCount = newActivityCount;
						newActivityCount = newActivityCount + 10;
						String updateActivityCall= ConnectionFunctions.FUNCTIONBASEURL + "getUserActivity_userid_from_to.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null)+"&from="+activityCount+"&to="+newActivityCount;
						new ListViewTask("load_activity").execute(updateActivityCall);
						ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Loading activities...", true, false);
						
					}
				}
			}
			
		});
		
		adapter = new ActivityListAdapter(getActivity(), activities);
		activities_list.setAdapter(adapter);
		
		postQuestionInput =	(EditText) rootView.findViewById(R.id.postQuestionInput);
		
		postQuestionButton = (Button) rootView.findViewById(R.id.postQuestionButton);
		postQuestionButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(!postQuestionInput.getText().toString().equals("")) {
					String question = postQuestionInput.getText().toString().replaceAll(" ", "%20");
					ConnectionFunctions.pdLoadFeed = ProgressDialog.show(getActivity(), "", "Posting question...", true, false);
					String setQuestionCall = ConnectionFunctions.FUNCTIONBASEURL + "setQuestion_userid_target_content.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null)+"&target=0"+"&content="+question;
					new ConnectionTask(getActivity(), "post_activity").execute(setQuestionCall);	
				}
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(postQuestionInput.getWindowToken(), 0);
			}
		});
		
		return rootView;
	}
	
	private void postCheck(String task, String result) {
		try {
			// Returns first 10 activities
			if(task.equals("load_activity")) {
			    jArr = new JSONArray(result);
			    StringFormatter s = new StringFormatter();

				for(int i=0; i<jArr.length();i++) {
					//JSONArray jArr2 = jArr.getJSONArray(i);
					ActivityItem a = new ActivityItem();
					a.setId(jArr.getJSONObject(i).getString("id"));
					a.setType(jArr.getJSONObject(i).getString("app"));
					a.setIcon(jArr.getJSONObject(i).getString("avatar"));
					String activityFormat = s.format(jArr.getJSONObject(i).getString("title"));
					a.setActivity(activityFormat);
					a.setTime(jArr.getJSONObject(i).getString("created"));
					a.setCreatorLink(jArr.getJSONObject(i).getString("actor"));
					a.setPostLink(jArr.getJSONObject(i).getString("cid"));
					a.setTargetLink(jArr.getJSONObject(i).getString("target"));
					activities.add(a);
				}
				adapter.notifyDataSetChanged();
			}
			
			else if(task.equals("set_work")) {
				String updateMeCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserOtherInfo_userid.php?userid="+userSettings.getString(ConnectionFunctions.USERID, null);
				new ConnectionTask(getActivity(), "open_me").execute(updateMeCall);
			}
			
		} catch (JSONException e) {
			
		}
		finally {
			if(ConnectionFunctions.pdLoadFeed != null)
				ConnectionFunctions.pdLoadFeed.dismiss();
		}
	}

	// Have to re call Connection Task from within to save lists, adapter
	// and Listview content.  Cannot figure out a way to get the content
	// from ConenctionFunction.  Will check later.
	private class ListViewTask extends AsyncTask<String, Void, String> {
		String task = "";
		StringBuilder sb;
		SSLContext context;
		
		public ListViewTask(String t) {
			task = t;
			sb = new StringBuilder();
			
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				InputStream caInput = getActivity().getResources().openRawResource(R.raw.hivecert);
				java.security.cert.Certificate ca;
				try {
					ca = cf.generateCertificate(caInput);
					System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
				} finally {
					caInput.close();
				}
			
				//Create a KeyStore containing our trusted CAs
				String keyStoreType = KeyStore.getDefaultType();
				KeyStore keyStore = KeyStore.getInstance(keyStoreType);
				keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca);
			
				//Create a TrustManager that trusts the CAs in our KeyStore
				String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
				TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
				tmf.init(keyStore);
			
				//Create an SSLContext that uses our TrustManager
				context = SSLContext.getInstance("TLS");
				context.init(null, tmf.getTrustManagers(), null);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				URL url = new URL(params[0]);
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setSSLSocketFactory(context.getSocketFactory());
				InputStream inStream = urlConnection.getInputStream();
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					if(line != null) {
						sb.append(line + "/n");
					}
				}
				bufferedReader.close();
				inStream.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		@Override
		protected void onPostExecute(String result) {
			postCheck(task, result);
			super.onPostExecute(result);
		}	
	}
	
}
