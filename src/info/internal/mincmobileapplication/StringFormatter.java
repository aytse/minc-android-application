package info.internal.mincmobileapplication;

import android.text.Html;

public class StringFormatter {

	public static String format(String string) {
		
		string = string.replace("\\n", "\n");
		string = Html.fromHtml(string).toString();
		
		return string;
		
	}
	
}
