package info.internal.mincmobileapplication;

import info.main.mincmobileapplication.R;

import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

public class DownloadImagesTask extends AsyncTask <ImageView, Void, Bitmap> {
	
	Activity activity;
	ImageView imageView = null;
	SSLContext context;
	
	public DownloadImagesTask(Activity a) {
		activity = a;
		
		try {
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			InputStream caInput = activity.getResources().openRawResource(R.raw.hivecert);
			java.security.cert.Certificate ca;
			try {
				ca = cf.generateCertificate(caInput);
				System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
			} finally {
				caInput.close();
			}
		
			//Create a KeyStore containing our trusted CAs
			String keyStoreType = KeyStore.getDefaultType();
			KeyStore keyStore = KeyStore.getInstance(keyStoreType);
			keyStore.load(null, null);
			keyStore.setCertificateEntry("ca", ca);
		
			//Create a TrustManager that trusts the CAs in our KeyStore
			String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
			tmf.init(keyStore);
		
			//Create an SSLContext that uses our TrustManager
			context = SSLContext.getInstance("TLS");
			context.init(null, tmf.getTrustManagers(), null);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

	@Override
	protected Bitmap doInBackground(ImageView... imageViews) {
		this.imageView = imageViews[0];
		return download_Image((String)imageView.getTag());
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		imageView.setImageBitmap(result);
	}
	
	private Bitmap download_Image(String url) {
		Bitmap bmp = null;
		try {
			URL ulrn = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection)ulrn.openConnection();
			con.setSSLSocketFactory(context.getSocketFactory());
			InputStream is = con.getInputStream();
			bmp = BitmapFactory.decodeStream(is);
			if (null != bmp)
				return bmp;
		} catch(Exception e) {}
		return bmp;
	}
}
