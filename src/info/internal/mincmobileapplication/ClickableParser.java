package info.internal.mincmobileapplication;

import info.internal.mincmobileapplication.ClickableTextView.ClickableWord;

import java.util.ArrayList;

import android.app.Activity;
import android.text.style.ClickableSpan;

public class ClickableParser {
	private String displayString;
	
	public ArrayList<ClickableWord> toParse (Activity activity, String str) {
		StringBuilder stringBuilder = new StringBuilder();
		
		String[] parts = str.split("#ll#");
		
		ArrayList<ClickableWord> clickableWords = new ArrayList<ClickableWord>();
		
		if(parts.length<3) {
			
			//stringBuilder.append(parts[0]);
			
			for(int i=0;i<parts.length;i++) {
				/*
				 * Formula is:
				 * "Report on #ll#[subject]#rtype#[type]#rid#[id]#ll#
				 */
				String[]subParts = parts[i].split("#\\|\\|#");
				// Every odd link is clickable
				if(i % 2 == 1) {
					String clickableLink = subParts[0];
					String clickableType = subParts[2];
					String clickableId;
					if(subParts.length<5) {
						clickableId = "";
					}
					else
						clickableId = subParts[4];
					
					ClickableSpan clickableSpan = null;
					ClickableWord c = new ClickableWord();
				
					clickableSpan = new ClickableSpanActivity(activity, clickableId, clickableType, clickableLink);
				
					c.setWord(clickableLink);
					c.setClickableSpan(clickableSpan);
				
					clickableWords.add(c);
				}
				
				stringBuilder.append(subParts[0]);
				
			}
		}
		else {
			for(int i=1; i<parts.length; i++) {
				/*
			 	* Formula is: 
			 	* #ll#[Subject]#rtype#[type]#rid#[id]#ll# [verb] #ll#[Post]
			 	* #rtype#[type]#rid#[id]#ll# [preposition]#ll#[destination]
			 	* #rtype#[type]#rid#[id]#ll# [end]
			 	*/
				// Split clickable links like:
				// [Subject]#rtype#[type]#rid#[id]
				String[] subParts = parts[i].split("#\\|\\|#");
				// Every odd link is a clickable link
				if(i % 2 == 1) {
					String clickableLink = subParts[0];
					String clickableType = subParts[2];
					String clickableId;
					if(subParts.length<5) {
						clickableId = "";
					}
					else
						clickableId = subParts[4];
				
					ClickableSpan clickableSpan = null;
					ClickableWord c = new ClickableWord();
				
					clickableSpan = new ClickableSpanActivity(activity, clickableId, clickableType, clickableLink);
				
					c.setWord(clickableLink);
					c.setClickableSpan(clickableSpan);
				
					clickableWords.add(c);
				}	
			
				stringBuilder.append(subParts[0]);
			
			}
		}
		displayString = stringBuilder.toString();
		return clickableWords;
	}
	
	public String getString() {
		return displayString;
	}
	
}
