package info.internal.mincmobileapplication;

import info.main.mincmobileapplication.CourseFolderFragment;
import info.main.mincmobileapplication.MainActivity;
import info.server.mincmobileapplication.ConnectionFunctions;
import info.server.mincmobileapplication.ConnectionTask;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.style.ClickableSpan;
import android.view.View;

public class ClickableSpanActivity extends ClickableSpan{
	private String id, type, link;
	private Activity activity;
	
	SharedPreferences userSettings;
	Editor userSettingsEditor;
	
	public ClickableSpanActivity() {}
	
	public ClickableSpanActivity(Activity activity, String id, String type, String link) {
		userSettings = activity.getApplicationContext().getSharedPreferences("UserPreferences", 0);
		userSettingsEditor = userSettings.edit();
		
		this.activity=activity;
		this.id = id;
		this.type = type;
		this.link = link;
	}
	
	@Override
	public void onClick(View widget) {
		userSettingsEditor.putString(ConnectionFunctions.SELECTEDID, id);
		userSettingsEditor.commit();
		if(type.equals("user")) {
			String openUserCall = ConnectionFunctions.FUNCTIONBASEURL+"getUserOtherInfo_userid.php?userid="+userSettings.getString(ConnectionFunctions.SELECTEDID, null);
			new ConnectionTask(activity, "open_user").execute(openUserCall);
			ConnectionFunctions.pdLoadFeed = ProgressDialog.show(activity, "", "Loading user...", true, false);
		}
		else if(type.equals("question")) {
			userSettingsEditor.putString(ConnectionFunctions.QUESTIONID, id);
			userSettingsEditor.commit();
			String openQuestionCall = ConnectionFunctions.FUNCTIONBASEURL+"getQuestion_questionid_userid.php?questionid="+
					userSettings.getString(ConnectionFunctions.QUESTIONID, null)+"&userid="+
					userSettings.getString(ConnectionFunctions.USERID, null);
			new ConnectionTask(activity, "open_question").execute(openQuestionCall);
			ConnectionFunctions.pdLoadFeed = ProgressDialog.show(activity, "", "Loading question...", true, false);
		}
		else if(type.equals("course")) {
			userSettingsEditor.putString(ConnectionFunctions.COURSEID, id);
			userSettingsEditor.commit();
			String openCourseCall = ConnectionFunctions.FUNCTIONBASEURL+"getCourseInfo_courseid_userid.php?courseid="+
					userSettings.getString(ConnectionFunctions.COURSEID, null)+"&userid="+
					userSettings.getString(ConnectionFunctions.USERID, null);
			new ConnectionTask(activity, "open_course").execute(openCourseCall);
			ConnectionFunctions.pdLoadFeed = ProgressDialog.show(activity, "", "Loading course...", true, false);
		}
		// NEEDS COLLECTION CLICKABLESPAN
		else if(type.equals("collection")) {
			userSettingsEditor.putString(ConnectionFunctions.FOLDERID, id);
			userSettingsEditor.putString(ConnectionFunctions.FOLDERNAME, link);
			userSettingsEditor.commit();
			((MainActivity) activity).changeFragment(new CourseFolderFragment(), userSettings.getString(ConnectionFunctions.FOLDERNAME, null));
		}
		
		// NEEDS DOCUMENT CLICKABLESPAN
		else if(type.equals("document")) {
			userSettingsEditor.putString(ConnectionFunctions.DOCUMENTID, id);
			userSettingsEditor.putString(ConnectionFunctions.DOCUMENTNAME, link);
			userSettingsEditor.commit();
			String openDocumentCall = ConnectionFunctions.FUNCTIONBASEURL+"getDocumentURL_rid.php?rid="+
			userSettings.getString(ConnectionFunctions.DOCUMENTID, null);
			new ConnectionTask(activity, "open_document").execute(openDocumentCall);
			ConnectionFunctions.pdLoadFeed = ProgressDialog.show(activity, "", "Loading document...", true, false);
		}
		// NEEDS GROUP CLICKABLESPAN
		else if(type.equals("group")) {
			userSettingsEditor.putString(ConnectionFunctions.GROUPID, id);
			userSettingsEditor.commit();
			String openGroupCall = ConnectionFunctions.FUNCTIONBASEURL+"getGroupInfo_groupid_userid.php?groupid="+userSettings.getString(ConnectionFunctions.GROUPID, null)+
					"&userid="+userSettings.getString(ConnectionFunctions.USERID, null);
			new ConnectionTask(activity, "open_group").execute(openGroupCall);
			ConnectionFunctions.pdLoadFeed = ProgressDialog.show(activity, "", "Loading group...", true, false);
		}
	}

}
