package info.model.mincmobileapplication;

public class ElementItem {
	private String id, name, avatar, type;
	
	public ElementItem(){}
	
	/*public ElementItem(String id, String name, String avatar) {
		this.id=id;
		this.name=name;
		this.avatar=avatar;
	}*/
	
	public ElementItem(String id, String name, String avatar, String type) {
		this.id=id;
		this.name=name;
		this.avatar=avatar;
		this.type = type;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getAvatar() {
		return this.avatar;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void setId(String id) {
		this.id=id;
	}
	
	public void setName(String name) {
		this.name=name;
	}
	
	public void setAvatar(String avatar) {
		this.avatar=avatar;
	}
	
	public void setType(String type) {
		this.type=type;
	}
}
