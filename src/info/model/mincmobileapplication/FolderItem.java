package info.model.mincmobileapplication;

public class FolderItem {
	private String id, name, icon, url, description;
	
	public FolderItem() {}
	
	public FolderItem(String id, String name, String icon, 
			String url, String description) {
		this.id = id;
		this.name = name;
		this.icon = icon;
		this.url = url;
		this.description = description;
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getIcon() {
		return icon;
	}
	
	public String getUrl() {
		return url;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
