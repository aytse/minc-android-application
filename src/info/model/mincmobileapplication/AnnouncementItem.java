package info.model.mincmobileapplication;

public class AnnouncementItem {
	private String id, title, message, date;
	
	public AnnouncementItem() {}
	
	public AnnouncementItem(String id, String title, String message,
			String date) {
		this.id = id;
		this.title = title;
		this.message = message;
		this.date = date;
	}
	
	public String getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
}
