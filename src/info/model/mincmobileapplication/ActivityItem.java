package info.model.mincmobileapplication;

public class ActivityItem {
	private String id, type, icon, activity, 
	time, creatorLink, postLink, targetLink;
	
	public ActivityItem(){}
	
	//Activity with creator, target, and post
	public ActivityItem(String id, String type, String icon, String activity, String time, String creatorLink, String postLink, String targetLink) {
		this.id=id;
		this.type=type;
		this.icon=icon;
		this.activity=activity;
		this.time=time;
		this.creatorLink=creatorLink;
		this.postLink=postLink;
		this.targetLink=targetLink;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getType() {
		return this.type;
	}
	
	public String getIcon() {
		return this.icon;
	}
	
	public String getActivity() {
		return this.activity;
	}
	
	public String getTime() {
		return this.time;
	}
	
	public String getCreatorLink() {
		return this.creatorLink;
	}
	
	public String getPostLink() {
		return this.postLink;
	}
	
	public String getTargetLink() {
		return this.targetLink;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	
	public void setCreatorLink(String creatorLink) {
		this.creatorLink = creatorLink;
	}
	
	public void setPostLink(String postLink) {
		this.postLink = postLink;
	}
	
	public void setTargetLink(String targetLink) {
		this.targetLink = targetLink;
	}
}
