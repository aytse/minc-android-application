package info.model.mincmobileapplication;

public class ReplyItem {
	private String replyId;
	private String replierId;
	private String replierName;
	private String replierIcon;
	private String reply;
	private String replyTime;
	private String replyAverageRate;
	private String replyNumberRate;
	private String replyMyRate;
	
	public ReplyItem() {}
	
	public ReplyItem(String replyId, String replierId, String replierName,
			String replierIcon, String reply, String replyTime,
			String replyAverageRate, String replyNumberRate, String replyMyRate) {
		this.replyId = replyId;
		this.replierId = replierId;
		this.replierName = replierName;
		this.replierIcon = replierIcon;
		this.reply = reply;
		this.replyTime = replyTime;
		this.replyAverageRate = replyAverageRate;
		this.replyNumberRate = replyNumberRate;
		this.replyMyRate = replyMyRate;
	}
	
	public String getReplyId() {
		return this.replyId;
	}
	
	public String getReplierId() {
		return this.replierId;
	}
	
	public String getReplierName() {
		return this.replierName;
	}
	
	public String getReplierIcon() {
		return this.replierIcon;
	}
	
	public String getReply() {
		return this.reply;
	}
	
	public String getReplyTime() {
		return this.replyTime;
	}
	
	public String getReplyAverageRate() {
		return this.replyAverageRate;
	}
	
	public String getReplyNumberRate() {
		return this.replyNumberRate;
	}
	
	public String getReplyMyRate() {
		return this.replyMyRate;
	}
	
	public void setReplyId(String replyId) {
		this.replyId = replyId;
	}
	
	public void setReplierId(String replierId) {
		this.replierId = replierId;
	}
	
	public void setReplierName(String replierName) {
		this.replierName = replierName;
	}
	
	public void setReplierIcon(String replierIcon) {
		this.replierIcon = replierIcon;
	}
	
	public void setReply(String reply) {
		this.reply = reply;
	}
	
	public void setReplyTime(String replyTime) {
		this.replyTime = replyTime;
	}
	
	public void setReplyAverageRate(String replyAverageRate) {
		this.replyAverageRate = replyAverageRate;
	}
	
	public void setReplyNumberRate(String replyNumberRate) {
		this.replyNumberRate = replyNumberRate;
	}
	
	public void setReplyMyRate(String replyMyRate) {
		this.replyMyRate = replyMyRate;
	}
}
