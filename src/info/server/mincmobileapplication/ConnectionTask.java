package info.server.mincmobileapplication;

import info.main.mincmobileapplication.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import android.app.Activity;
import android.os.AsyncTask;

public class ConnectionTask extends AsyncTask<String, Void, String> {
	String task = "";
	Activity activity;
	StringBuilder sb;
	SSLContext context;
	
	public ConnectionTask(Activity a, String t) {
		activity = a;
		task = t;
		sb = new StringBuilder();
		
		//Google Implementation
		//Load CAs from an InputStream
		//(could be from a resource or ByteArrayInputStream or ...)
		try {
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			InputStream caInput = activity.getResources().openRawResource(R.raw.hivecert);
			java.security.cert.Certificate ca;
			try {
				ca = cf.generateCertificate(caInput);
				System.out.println("ca="+ ((X509Certificate) ca).getSubjectDN());
			} finally {
				caInput.close();
			}
		
			//Create a KeyStore containing our trusted CAs
			String keyStoreType = KeyStore.getDefaultType();
			KeyStore keyStore = KeyStore.getInstance(keyStoreType);
			keyStore.load(null, null);
			keyStore.setCertificateEntry("ca", ca);
		
			//Create a TrustManager that trusts the CAs in our KeyStore
			String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
			tmf.init(keyStore);
		
			//Create an SSLContext that uses our TrustManager
			context = SSLContext.getInstance("TLS");
			context.init(null, tmf.getTrustManagers(), null);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

	@Override
	protected String doInBackground(String... params) {
		try {
			//Original with normal http
			//HttpClient httpClient = new DefaultHttpClient();
			//HttpPost httpPost = new HttpPost(params[0]);
			//HttpResponse httpResponse = httpClient.execute(httpPost);
			
			//Google Implementation
			//Tell the URLConnection to use a SocketFactory from our SSLContext
			URL url = new URL(params[0]);
			HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
			urlConnection.setSSLSocketFactory(context.getSocketFactory());
			InputStream inStream = urlConnection.getInputStream();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inStream, "iso-8859-1"), 8);
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				if(line != null) {
					sb.append(line + "/n");
				}
			}
			bufferedReader.close();
			inStream.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();

	}

	@Override
	protected void onPostExecute(String result) {
		ConnectionFunctions cf;
		cf = new ConnectionFunctions(activity, task, result);
		
		cf.postCheck();
		super.onPostExecute(result);
	}	
	
}

