package info.server.mincmobileapplication;

import info.adapter.mincmobileapplication.CourseListAdapter;
import info.internal.mincmobileapplication.StringFormatter;
import info.main.mincmobileapplication.CourseFragment;
import info.main.mincmobileapplication.DescriptionFragment;
import info.main.mincmobileapplication.GroupFragment;
import info.main.mincmobileapplication.LoginActivity;
import info.main.mincmobileapplication.MainActivity;
import info.main.mincmobileapplication.QuestionFragment;
import info.main.mincmobileapplication.UserFragment;
import info.model.mincmobileapplication.ElementItem;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.NetworkOnMainThreadException;
import android.widget.Toast;

public class ConnectionFunctions {
	//public static final String FUNCTIONBASEURL = "http://54.186.210.101/minc/PHP_Functions/";
	public static final String FUNCTIONBASEURL = "https://hive.asu.edu/minc/PHP_Functions/";
	public static final String USERID = "UserId";
	public static final String NAME = "Name";
	public static final String WORK = "Work";
	public static final String ABOUTME = "AboutMe";
	public static final String POSITION = "Postion";
	public static final String AFFILIATION = "Affiliation";
	public static final String AVATAR = "Avatar";
	public static final String GENDER = "Gender";
	public static final String LAND = "Land";
	public static final String MOBILE = "Mobile";
	public static final String GRADUATE = "Graduate";
	public static final String ADDRESS = "Address";
	public static final String STATE = "State";
	public static final String CITY = "City";
	public static final String COUNTRY = "Country";
	public static final String WEBSITE = "Website";
	public static final String COLLEGE = "College";
	public static final String INTEREST = "Interest";
	public static final String EXPERT = "Expert";
	public static final String INTERMEDIATE = "Intermediate";
	public static final String BEGINNER = "Beginner";
	public static final String OTHERS = "Others";
	public static final String PEOPLE_LOGGED_IN = "PeopleLoggedIn";
	public static final String PEOPLE_LOGGED_OUT = "PeopleLoggedOut";

	public static Boolean MYPROFILE = false;
	public static Boolean backPressed = true;

	// Course returns
	public static final String COURSEID = "CourseId";
	public static final String COURSENAME = "CourseName";
	public static final String COURSEAVATAR = "CourseAvatar";
	public static final String COURSEYEAR = "CourseYear";
	public static final String COURSETERM = "CourseTerm";
	public static final String COURSESTART = "CourseStart";
	public static final String COURSEEND = "CourseEnd";
	public static final String COURSELOCATION = "CourseLocation";
	public static final String COURSEHASHTAG = "CourseHashTag";
	public static final String COURSEDESCRIPTION = "CourseDescription";
	public static final String COURSEMYRATING = "CourseMyRating";
	public static final String COURSEAVGRATING = "CourseAvgRating";
	public static final String COURSENUMBERRATE = "CourseNumberRate";

	public static final String GROUPID = "GroupId";
	public static final String GROUPNAME = "GroupName";
	public static final String GROUPAVATAR = "GroupAvatar";
	public static final String GROUPCREATED = "GroupCreated";
	public static final String GROUPCREATOR = "GroupCreator";
	public static final String GROUPDESCRIPTION = "GroupDescription";
	public static final String GROUPMYRATING = "GroupMyRating";
	public static final String GROUPAVGRATING = "GroupAvgRating";
	public static final String GROUPNUMBERRATE = "GroupNumberRate";
	
	public static final String ANNOUNCEMENTTITLE = "AnnouncementTitle";
	public static final String ANNOUNCEMENTMESSAGE = "AnnouncementMessage";

	// Question returns
	public static final String QUESTIONID = "QuestionId";
	public static final String QUESTIONTITLE = "QuestionTitle";
	public static final String QUESTIONNAME = "QuestionName";
	public static final String ASKERID = "AskerId";
	public static final String ASKERNAME = "AskerName";
	public static final String ASKERAVATAR = "AskerAvatar";
	public static final String QUESTION = "Question";
	public static final String QUESTIONTIME = "QuestionTime";
	public static final String QUESTIONAVGRATING = "QuestionAvgRating";
	public static final String QUESTIONMYRATING = "QuestionMyRating";
	public static final String QUESTIONNUMBERRATE = "QuestionNumberRate";

	// User returns
	public static final String SELECTEDID = "SelectedId";
	public static final String SELECTEDNAME = "SelectedName";
	public static final String SELECTEDWORK = "SelectedWork";
	public static final String SELECTEDABOUTME = "SelectedAboutMe";
	public static final String SELECTEDPOSITION = "SelectedPostion";
	public static final String SELECTEDAFFILIATION = "SelectedAffiliation";
	public static final String SELECTEDAVATAR = "SelectedAvatar";
	public static final String SELECTEDGENDER = "SelectedGender";
	public static final String SELECTEDLAND = "SelectedLand";
	public static final String SELECTEDMOBILE = "SelectedMobile";
	public static final String SELECTEDGRADUATE = "SelectedGraduate";
	public static final String SELECTEDADDRESS = "SelectedAddress";
	public static final String SELECTEDSTATE = "SelectedState";
	public static final String SELECTEDCITY = "SelectedCity";
	public static final String SELECTEDCOUNTRY = "SelectedCountry";
	public static final String SELECTEDWEBSITE = "SelectedWebsite";
	public static final String SELECTEDCOLLEGE = "SelectedCollege";
	public static final String SELECTEDINTEREST = "SelectedInterest";
	public static final String SELECTEDEXPERT = "SelectedExpert";
	public static final String SELECTEDINTERMEDIATE = "SelectedIntermediate";
	public static final String SELECTEDBEGINNER = "SelectedBeginner";
	public static final String SELECTEDOTHERS = "SelectedOthers";

	public static final String FOLDERID = "FolderId";
	public static final String FOLDERNAME = "FolderName";

	public static final String DOCUMENTID = "DocumentId";
	public static final String DOCUMENTNAME = "DocumentName";
	public static final String DOCUMENTURL = "DocumentUrl";
	public static final String DESCRIPTION = "Description";

	// Activity returns
	public static ArrayList<ElementItem> courses, allCourses, connections;

	public static String[] ratings = new String[] { "1", "2", "3", "4", "5" };

	public static String[] levels = new String[] { "Beginner", "Intermediate",
			"Expert", "Other" };

	JSONArray jArr, all_jArr;
	public static ProgressDialog pdLoadFeed;
	SharedPreferences userSettings;
	Editor userSettingsEditor;

	String task;
	String results;
	Activity activity;

	public static CourseListAdapter connectionAdapter;

	public ConnectionFunctions() {
	}

	public ConnectionFunctions(Activity activity, String task, String results) {
		userSettings = activity.getApplicationContext().getSharedPreferences(
				"UserPreferences", 0);
		userSettingsEditor = userSettings.edit();

		this.activity = activity;
		this.task = task;
		this.results = results;
	}

	public void postCheck() {
		try {
			if (task.equals("Login")) {
				JSONObject jObj = new JSONArray(results).getJSONObject(0);
				if (jObj.getBoolean("chk")) {
					userSettingsEditor.putString(USERID, jObj.getString("id"));
					userSettingsEditor.putString(NAME, jObj.getString("name"));
					userSettingsEditor.putString(WORK,
							jObj.getString("checkinstatus"));
					userSettingsEditor.putString(AVATAR,
							jObj.getString("avatar"));
					userSettingsEditor.putString(GENDER,
							jObj.getString("gender"));
					userSettingsEditor.putString(MOBILE,
							jObj.getString("phone"));
					userSettingsEditor.putString(GRADUATE,
							jObj.getString("graduate"));
					userSettingsEditor.putString(COLLEGE,
							jObj.getString("affiliation"));
					userSettingsEditor.putBoolean(PEOPLE_LOGGED_IN, true);
					userSettingsEditor.putBoolean(PEOPLE_LOGGED_OUT, false);
					userSettingsEditor.commit();
					Intent i = new Intent(activity, MainActivity.class);
					activity.startActivity(i);
				} else
					Toast.makeText(activity,
							"Invalid username/password, please try again.",
							Toast.LENGTH_SHORT).show();
				pdLoadFeed.dismiss();
			}

			else if (task.equals("open_me")) {
				JSONObject jObj = new JSONArray(results).getJSONObject(0);
				userSettingsEditor.putString(NAME, jObj.getString("name"));
				userSettingsEditor.putString(WORK, jObj.getString("work"));
				userSettingsEditor
						.putString(ABOUTME, jObj.getString("aboutme"));
				userSettingsEditor.putString(POSITION,
						jObj.getString("position"));
				userSettingsEditor.putString(AFFILIATION,
						jObj.getString("affiliation"));
				userSettingsEditor.putString(AVATAR, jObj.getString("avatar"));
				userSettingsEditor.putString(GENDER, jObj.getString("gender"));
				userSettingsEditor.putString(LAND, jObj.getString("landphone"));
				userSettingsEditor.putString(MOBILE, jObj.getString("mobile"));
				userSettingsEditor.putString(GRADUATE,
						jObj.getString("graduate"));
				userSettingsEditor
						.putString(ADDRESS, jObj.getString("address"));
				userSettingsEditor.putString(STATE, jObj.getString("state"));
				userSettingsEditor.putString(CITY, jObj.getString("city"));
				userSettingsEditor
						.putString(COUNTRY, jObj.getString("country"));
				userSettingsEditor
						.putString(WEBSITE, jObj.getString("website"));
				userSettingsEditor
						.putString(COLLEGE, jObj.getString("college"));
				userSettingsEditor.putString(INTEREST,
						jObj.getString("Interest"));
				userSettingsEditor.putString(EXPERT, jObj.getString("Expert"));
				userSettingsEditor.putString(INTERMEDIATE,
						jObj.getString("Intermediate"));
				userSettingsEditor.putString(BEGINNER,
						jObj.getString("Beginner"));
				userSettingsEditor.putString(OTHERS, jObj.getString("Others"));
				userSettingsEditor.commit();
				pdLoadFeed.dismiss();
			}

			else if (task.equals("open_question")) {
				JSONObject jObj = new JSONArray(results).getJSONObject(0);

				userSettingsEditor.putString(QUESTIONID,
						jObj.getString("questionid"));
				userSettingsEditor.putString(QUESTIONNAME,
						jObj.getString("title"));
				userSettingsEditor.putString(ASKERNAME, jObj.getString("name"));
				userSettingsEditor
						.putString(ASKERID, jObj.getString("creator"));
				userSettingsEditor.putString(ASKERAVATAR,
						jObj.getString("avatar"));
				String questionFormat = StringFormatter.format(jObj.getString("message"));
				userSettingsEditor.putString(QUESTION, questionFormat);
				userSettingsEditor.putString(QUESTIONTIME,
						jObj.getString("lastreplied"));
				userSettingsEditor.putString(QUESTIONMYRATING,
						jObj.getString("myrating"));
				userSettingsEditor.putString(QUESTIONAVGRATING,
						jObj.getString("avgrating"));
				userSettingsEditor.putString(QUESTIONNUMBERRATE,
						jObj.getString("totcount"));
				userSettingsEditor.commit();
				pdLoadFeed.dismiss();
				((MainActivity) activity).changeFragment(
						new QuestionFragment(),
						userSettings.getString(QUESTIONNAME, null));
			}

			else if (task.equals("open_user")) {
				JSONObject jObj = new JSONArray(results).getJSONObject(0);
				userSettingsEditor.putString(SELECTEDNAME,
						jObj.getString("name"));
				userSettingsEditor.putString(SELECTEDWORK,
						jObj.getString("work"));
				userSettingsEditor.putString(SELECTEDABOUTME,
						jObj.getString("aboutme"));
				userSettingsEditor.putString(SELECTEDPOSITION,
						jObj.getString("position"));
				userSettingsEditor.putString(SELECTEDAFFILIATION,
						jObj.getString("affiliation"));
				userSettingsEditor.putString(SELECTEDAVATAR,
						jObj.getString("avatar"));
				userSettingsEditor.putString(SELECTEDGENDER,
						jObj.getString("gender"));
				userSettingsEditor.putString(SELECTEDLAND,
						jObj.getString("landphone"));
				userSettingsEditor.putString(SELECTEDMOBILE,
						jObj.getString("mobile"));
				userSettingsEditor.putString(SELECTEDGRADUATE,
						jObj.getString("graduate"));
				userSettingsEditor.putString(SELECTEDADDRESS,
						jObj.getString("address"));
				userSettingsEditor.putString(SELECTEDSTATE,
						jObj.getString("state"));
				userSettingsEditor.putString(SELECTEDCITY,
						jObj.getString("city"));
				userSettingsEditor.putString(SELECTEDCOUNTRY,
						jObj.getString("country"));
				userSettingsEditor.putString(SELECTEDWEBSITE,
						jObj.getString("website"));
				userSettingsEditor.putString(SELECTEDCOLLEGE,
						jObj.getString("college"));
				userSettingsEditor.putString(SELECTEDINTEREST,
						jObj.getString("Interest"));
				userSettingsEditor.putString(SELECTEDEXPERT,
						jObj.getString("Expert"));
				userSettingsEditor.putString(SELECTEDINTERMEDIATE,
						jObj.getString("Intermediate"));
				userSettingsEditor.putString(SELECTEDBEGINNER,
						jObj.getString("Beginner"));
				userSettingsEditor.putString(SELECTEDOTHERS,
						jObj.getString("Others"));
				userSettingsEditor.commit();
				pdLoadFeed.dismiss();
				MYPROFILE = false;
				((MainActivity) activity).changeFragment(new UserFragment(),
						userSettings.getString(SELECTEDNAME, null));
			}

			else if (task.equals("open_course")) {
				JSONObject jObj = new JSONArray(results).getJSONObject(0);
				userSettingsEditor
						.putString(COURSENAME, jObj.getString("name"));
				userSettingsEditor.putString(COURSEAVATAR,
						jObj.getString("avatar"));
				userSettingsEditor
						.putString(COURSEYEAR, jObj.getString("year"));
				userSettingsEditor
						.putString(COURSETERM, jObj.getString("term"));
				userSettingsEditor.putString(COURSESTART,
						jObj.getString("start_time"));
				userSettingsEditor.putString(COURSEEND,
						jObj.getString("end_time"));
				userSettingsEditor.putString(COURSELOCATION,
						jObj.getString("location"));
				userSettingsEditor.putString(COURSEHASHTAG,
						jObj.getString("hashtag"));
				userSettingsEditor.putString(COURSEDESCRIPTION,
						jObj.getString("description"));
				userSettingsEditor.putString(COURSEMYRATING,
						jObj.getString("myrating"));
				userSettingsEditor.putString(COURSEAVGRATING,
						jObj.getString("avgrating"));
				userSettingsEditor.putString(COURSENUMBERRATE,
						jObj.getString("totcount"));
				userSettingsEditor.commit();
				pdLoadFeed.dismiss();
				((MainActivity) activity).changeFragment(new CourseFragment(),
						userSettings.getString(COURSENAME, null));
			}
			
			
			else if (task.equals("open_group")) {
				JSONObject jObj = new JSONArray(results).getJSONObject(0);
				userSettingsEditor.putString(GROUPNAME, jObj.getString("name"));
				userSettingsEditor.putString(GROUPAVATAR, jObj.getString("avatar"));
				userSettingsEditor.putString(GROUPDESCRIPTION, jObj.getString("description"));
				userSettingsEditor.putString(GROUPCREATOR, jObj.getString("groupcreator"));
				userSettingsEditor.putString(GROUPCREATED, jObj.getString("created"));
				userSettingsEditor.putString(GROUPMYRATING, jObj.getString("myrating"));
				userSettingsEditor.putString(GROUPAVGRATING, jObj.getString("avgrating"));
				userSettingsEditor.putString(GROUPNUMBERRATE, jObj.getString("totcount"));
				userSettingsEditor.commit();
				pdLoadFeed.dismiss();
				((MainActivity) activity).changeFragment(new GroupFragment(),
						userSettings.getString(GROUPNAME, null));
			}

			else if (task.equals("open_document")) {
				JSONObject jObj = new JSONArray(results).getJSONObject(0);
				userSettingsEditor.putString(DOCUMENTNAME,
						jObj.getString("title"));
				userSettingsEditor.putString(DESCRIPTION,
						jObj.getString("description"));
				userSettingsEditor.putString(DOCUMENTURL,
						jObj.getString("original"));
				userSettingsEditor.commit();
				pdLoadFeed.dismiss();

				if (!userSettings.getString(DESCRIPTION, null).equals("null")) {
					((MainActivity) activity).changeFragment(
							new DescriptionFragment(), userSettings.getString(
									ConnectionFunctions.DOCUMENTNAME, null));
				}

				else {
					try {
						String[] parts = userSettings.getString(DOCUMENTURL,
								null).split("http://hive.asu.edu/minc/");
						String link = parts[1];
						Intent intent = new Intent(Intent.ACTION_VIEW,
								Uri.parse(link));
						activity.startActivity(intent);
					} catch (ActivityNotFoundException e) {
						Toast.makeText(activity,
								"No application can handle this request",
								Toast.LENGTH_LONG).show();
						e.printStackTrace();
					}
				}
			}

			else if (task.equals("sign_out")) {
				pdLoadFeed.dismiss();
				userSettingsEditor.clear();
				userSettingsEditor.commit();
				Intent intent = new Intent(activity, LoginActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				activity.startActivity(intent);
			}

		} catch (JSONException e) {
			ConnectionFunctions.pdLoadFeed.dismiss();
			Toast.makeText(activity,
					"No results for "+task,
					Toast.LENGTH_SHORT).show();
		} catch (NetworkOnMainThreadException e) {
			Toast.makeText(activity, "Unable to connect to server.",
					Toast.LENGTH_SHORT).show();
		} finally {
			if (pdLoadFeed != null)
				pdLoadFeed.dismiss();
		}
	}
}
